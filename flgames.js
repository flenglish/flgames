var today = new Date();
var dd = String(today.getDate()).padStart(2, "0");
var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
var yyyy = today.getFullYear();
var allSavedGames = "";
var gameData;
var gameRawContent;
var toSave = "";
var lang = [];
var langGlobal = {};
var langLocal = {};

var darkOverlay;
var configDiv;
var dataDiv;
var dialogDiv;
var helpDiv;
var endDiv;
var timerDiv;
var gameTeamsDiv;
var actionDiv;
var selectTopicDiv;
var savedGamesDiv;

const allPlayers = [];
var selectedPlayers = [];
var absentPlayers = [];
var allTopics = [];
const allQuestions = [];
let selectedQuestions = [];

let setLanguage = getCookie("flgames-lang");

window.addEventListener("load", () => {
  let game;
  if (document.querySelector("body[data-game]")) {
    game = document.querySelector("body[data-game]").getAttribute("data-game");
  } else {
    game = false;
  }
  init(game);
});

function init(game = false) {
  if (setLanguage !== "") {
    if (document.title == "FLGames") {
      document.getElementById(setLanguage).classList.add("selected");
    }
  } else {
    setLanguage = "en";
    setCookie("flgames-lang", "en", 365);
    if (document.title == "FLGames") {
      document.getElementById("en").classList.add("selected");
    }
  }
  if (document.title == "FLGames") {
    if (setLanguage === "fr") {
      loadScript("./assets/data/lang.fr.js", setLang); // langGlobal
    } else {
      loadScript("./assets/data/lang.en.js", setLang); // langGlobal
    }
  } else {
    if (setLanguage === "fr") {
      loadScript("../../assets/data/lang.fr.js", setLang); // langGlobal
      loadScript("./assets/data/lang.fr.js", setLangs); // langLocal
    } else {
      loadScript("../../assets/data/lang.en.js", setLang); // langGlobal
      loadScript("./assets/data/lang.en.js", setLangs); // langLocal
    }
  }
  includeHTML(prepareDivs).then(() => {
    if (document.title == "FLGames") {
      loadSavedGames();
      loadAllGamesData().then(() => {
        loadClasses();
      });
    } else {
      if (game) {
        setTimeout(() => {
          loadClasses();
          loadTopics(game), 500;
        });
      } else {
        window.alert("[dev] : <body data-game='name-of-the-game'> not found.");
      }
      if (document.getElementById("today")) {
        document.getElementById("today").innerHTML = dd + "/" + mm + "/" + yyyy;
      }
    }
  });
}

var loadAllGamesData = async function () {
  document.querySelectorAll("div.thumbnail").forEach(function (el) {
    let game = el.getAttribute("data-game");
    if (game) {
      setTimeout(() => {
        loadData(game);
      }, 500);
    }
  });
};

var restoreData = function (game) {
  let file;
  if (confirm(lang["restoreData"])) {
    localStorage.removeItem(game + "Data");
    let dataTextArea = document.getElementById("myData-" + game);
    if (dataTextArea) {
      // Read from file (demo data ?)
      if (document.title == "FLGames") {
        // if (game != "myClasses") {
        //   file = "games/" + game + "/assets/data/topics.txt";
        // } else {
        //   file = "./assets/data/myClasses.txt";
        // }
        file = "./data/" + game + "-data.txt";
      } else {
        // file = "../../games/" + game + "/assets/data/topics.txt";
        file = "../../data/" + game + "-data.txt";
      }
      fetch(file)
        .then((res) => res.text())
        .then((text) => {
          dataTextArea.value = text;
          if (document.title != "FLGames") {
            if (game != "myClasses") {
              loadTopics(game);
            }
            toggle(dataDiv);
            toggle(configDiv);
          }
        });
    }
  }
};

var loadData = async function (game) {
  let file;
  gameData = localStorage.getItem(game + "Data");
  // See if local data textarea is here
  let dataTextArea = document.getElementById("myData-" + game);
  if (dataTextArea) {
    if (gameData != null && gameData != "") {
      // Read from local data
      dataTextArea.value = gameData;
      gameRawContent = gameData;
      if (document.getElementById("dataOrigin-" + game)) {
        document.getElementById("dataOrigin-" + game).innerHTML =
          lang["dataOriginLocal"];
      }
    } else {
      // Read from file (demo data)
      if (document.title == "FLGames") {
        file = "data/" + game + "-data.txt";
      } else {
        file = "../../data/" + game + "-data.txt";
      }
      await fetch(file)
        .then((res) => res.text())
        .then((text) => {
          dataTextArea.value = text;
          gameRawContent = text;
          if (document.getElementById("dataOrigin-" + game)) {
            document.getElementById("dataOrigin-" + game).innerHTML =
              lang["dataOriginFile"];
          }
        });
    }
    // Check if warning icon is here
    let queryName = "[data-game=" + game + "] .warningIcon";
    if (document.querySelector(queryName)) {
      if (gameData != null && gameData != "") {
        // Remove warning
        document.querySelector(queryName).classList.add("hidden");
      } else {
        // Add warning
        document.querySelector(queryName).classList.remove("hidden");
      }
    }
    return;
  }
};

var saveLocalData = function (game) {
  let gameData = document.getElementById("myData-" + game).value;
  localStorage.setItem(game + "Data", gameData);
  queryName = "[data-game=" + game + "] .warningIcon";
  if (document.querySelector(queryName)) {
    document.querySelector(queryName).classList.add("hidden");
  }
  toggle(dataDiv);
  if (document.title != "FLGames") {
    loadTopics(game);
    toggle(configDiv);
  }
};

var showLocalData = function (game) {
  toggle(dataDiv);
  document.querySelectorAll("div.dataFile").forEach(function (el) {
    el.classList.add("hidden");
  });
  let dataTextAreaDiv = document.getElementById(game + "DataDiv");
  if (dataTextAreaDiv) {
    dataTextAreaDiv.classList.remove("hidden");
  } else {
    window.alert("No local file with this name !");
  }
};

var setLang = async function () {
  Object.assign(lang, langGlobal);
  translate();
};
var setLangs = async function () {
  Object.assign(lang, langLocal);
  translate();
};

function prepareDivs() {
  darkOverlay = document.getElementById("darkOverlay");
  configDiv = document.getElementById("configDiv");
  dataDiv = document.getElementById("dataDiv");
  dialogDiv = document.getElementById("myDialog");
  helpDiv = document.getElementById("help");
  endDiv = document.getElementById("endDiv");
  timerDiv = document.getElementById("timer");
  gameTeamsDiv = document.getElementById("gameTeams");
  actionDiv = document.getElementById("actionDiv");
  selectTopicDiv = document.getElementById("topics");
  absentPlayersDiv = document.getElementById("displayAbsentPlayers");
  savedGamesDiv = document.getElementById("savedGames");
}

function saveLanguage(lang) {
  document.querySelectorAll(".flag").forEach(function (el) {
    el.classList.remove("selected");
  });
  document.getElementById(lang).classList.add("selected");
  setCookie("flgames-lang", lang, 365);
  window.location.reload();
}

function launchGame(name) {
  window.location.href = "./games/" + name + "/" + name + ".html";
}

function translate() {
  let allElements = document.querySelectorAll("[data-i18n]");
  for (let i = 0; i < allElements.length; i++) {
    let id = allElements[i].getAttribute("data-i18n");
    if (lang[id] || lang[id] == "") {
      if (lang[id] != "") {
        allElements[i].innerHTML = lang[id];
      } else {
        allElements[i].remove();
      }
    } else {
      allElements[i].innerHTML = id + " (todo)";
    }
  }
  let allTitles = document.querySelectorAll("[data-i18nt]");
  for (let i = 0; i < allTitles.length; i++) {
    let id = allTitles[i].getAttribute("data-i18nt");
    if (lang[id]) {
      if (lang[id] != "") {
        allTitles[i].setAttribute("title", lang[id]);
      } else {
        allTitles[i].removeAttribute("title");
      }
    } else {
      allTitles[i].setAttribute("title", id + " (todo)");
    }
  }
}

var parseMyClasses = function (text) {
  var lines = text.split("\n");
  var allGroups = [];
  for (var i = 0; i < lines.length; i++) {
    currentline = lines[i];
    if (
      currentline !== "" &&
      currentline.charAt(0) !== "/" &&
      currentline.charAt(1) !== "/"
    ) {
      let data = lines[i].split(",");
      let className = data[0];
      let objPlayer = {};
      objPlayer.team = data[0];
      objPlayer.name = data[1];
      objPlayer.id = i;
      objPlayer.isAbsent = 0;
      objPlayer.active = 1;
      objPlayer.score = 0;

      if (className !== "" && className !== "Class") {
        allPlayers.push(objPlayer);
      }
      if (allGroups.indexOf(className) === -1) {
        if (className !== "" && className !== "Class") {
          allGroups.push(className.trim());
          if (document.title == "FLGames") {
            let dataTextArea = document.getElementById("myData-myClasses");
            if (dataTextArea) {
              dataTextArea.value = text;
              if (document.getElementById("dataOrigin-myClasses")) {
                document.getElementById("dataOrigin-myClasses").innerHTML =
                  lang["dataOriginFile"];
              }
            }
          } else {
            document.getElementById("classList").innerHTML +=
              '<button class="btn" onclick="showTeam(\'' +
              className +
              "')\">" +
              className +
              "</button>";
          }
        }
      }
    }
  }
  if (document.title != "FLGames") {
    for (var i = 0; i < allPlayers.length; i++) {
      document.getElementById("playersList").innerHTML +=
        '<li class="teamPlayer" data-team="' +
        allPlayers[i].team +
        '"><input type="checkbox" class="cbPlayer" id="cb-' +
        allPlayers[i].id +
        '" onchange="toggleAbsent(' +
        allPlayers[i].id +
        ');"><label for="cb-' +
        allPlayers[i].id +
        '">' +
        allPlayers[i].name +
        "</label></input></li>";
    }
  }
  allPlayers.push({
    team: "no-class",
    name: lang["playerA"],
    isAbsent: 0,
    active: 1,
  });
  allPlayers.push({
    team: "no-class",
    name: lang["playerB"],
    isAbsent: 0,
    active: 1,
  });
};

async function loadClasses() {
  let file;
  classesData = localStorage.getItem("myClassesData");
  // See if local data textarea is here
  let dataTextArea = document.getElementById("myData-myClasses");
  if (classesData != null && classesData != "") {
    // Read from local data
    parseMyClasses(classesData);
    if (dataTextArea) {
      dataTextArea.value = classesData;
      if (document.getElementById("dataOrigin-myClasses")) {
        document.getElementById("dataOrigin-myClasses").innerHTML =
          lang["dataOriginLocal"];
      }
    }
  } else {
    // Read from file (demo data)
    if (document.title != "FLGames") {
      // file = "../../assets/data/myClasses.txt";
      file = "../../data/myClasses-data.txt";
    } else {
      file = "./data/myClasses-data.txt";
    }
    fetch(file)
      .then((res) => res.text())
      .then((text) => {
        parseMyClasses(text);
      })
      .catch((e) => console.error(e));
  }
}

async function loadTopics(game) {
  // Init data
  gameRawContent = "";
  allTopics = [];
  let lines, currentline, topic;
  if (document.getElementById("topicsList")) {
    document.querySelectorAll(".topicOption").forEach(function (el) {
      el.remove();
    });
  }
  loadData(game).then(() => {
    // Parse data
    switch (game) {
      case "tictactoe":
        lines = gameRawContent.split("\n");
        for (var i = 0; i < lines.length; i++) {
          let objQuestion = {};
          if (lines[i].charAt(0) === ":") {
            currentline = lines[i].replace(/^(:+\s*)+/g, ""); // Get rid of starting colon and following space
            topic = currentline.replace(/(\r\n|\n|\r)/gm, "").trim(); // Save topic title
            allTopics.push(topic);
          } else {
            objQuestion.topic = topic;
            currentline = lines[i]; // Save questions content
            if (
              currentline !== "" &&
              currentline.charAt(0) !== "/" &&
              currentline.charAt(1) !== "/"
            ) {
              // Add images links for [] markers
              // currentline = currentline.replace(
              //   /\[(.*)\]/g,
              //   "<img class='img-rounded' src='./assets/img/$1' alt='?' />",
              // );
              currentline = currentline.replace(
                /\[(.*)\]/g,
                "<img class='img-rounded' src='../../data/img/$1' alt='?' />",
              );
              objQuestion.question = currentline;
              allQuestions.push(objQuestion);
            }
          }
        }
        allTopics.sort();
        for (var i = 0; i < allTopics.length; i++) {
          document.getElementById("topicsList").innerHTML +=
            '<option class="topicOption" value="' +
            i +
            '">' +
            allTopics[i] +
            "</option></li>";
          document.getElementById("topicsList02").innerHTML +=
            '<option class="topicOption" value="' +
            i +
            '">' +
            allTopics[i] +
            "</option></li>";
        }
        break;
      case "soccer":
      case "writingfight":
        lines = gameRawContent.split("\n");
        for (var i = 0; i < lines.length; i++) {
          let objQuestion = {};
          if (lines[i].charAt(0) === ":") {
            currentline = lines[i].replace(/^(:+\s*)+/g, ""); // Get rid of starting colon and following space
            topic = currentline.replace(/(\r\n|\n|\r)/gm, "").trim(); // Save topic title
            allTopics.push(topic);
          } else {
            objQuestion.topic = topic;
            currentline = lines[i]; // Save questions content
            if (currentline !== "") {
              // Add images links for [] markers
              // currentline = currentline.replace(
              //   /\[(.*)\]/g,
              //   "<img class='img-rounded' src='./assets/img/$1' alt='?' />",
              // );
              currentline = currentline.replace(
                /\[(.*)\]/g,
                "<img class='img-rounded' src='../../data/img/$1' alt='?' />",
              );
              objQuestion.question = currentline;
              allQuestions.push(objQuestion);
            }
          }
        }
        allTopics.sort();
        for (var i = 0; i < allTopics.length; i++) {
          document.getElementById("topicsList").innerHTML +=
            '<option class="topicOption" value="' +
            i +
            '">' +
            allTopics[i] +
            "</option></li>";
          document.getElementById("topicsList02").innerHTML +=
            '<option class="topicOption" value="' +
            i +
            '">' +
            allTopics[i] +
            "</option></li>";
        }
        // return JSON.stringify(result); //JSON
        break;
      case "carrace":
        lines = gameRawContent.split("\n");
        for (var i = 0; i < lines.length; i++) {
          let objQuestion = {};
          if (lines[i].charAt(0) === ":") {
            currentline = lines[i].replace(/^(:+\s*)+/g, ""); // Get rid of starting colon and following space
            topic = currentline.replace(/(\r\n|\n|\r)/gm, "").trim(); // Save topic title
            allTopics.push(topic);
          } else {
            objQuestion.topic = topic;
            currentline = lines[i]; // Save questions content
            if (currentline !== "") {
              // Add images links for [] markers
              // currentline = currentline.replace(
              //   /\[(.*)\]/g,
              //   "<img class='img-rounded' src='./assets//img/$1' alt='?' />",
              // );
              currentline = currentline.replace(
                /\[(.*)\]/g,
                "<img class='img-rounded' src='../../data/img/$1' alt='?' />",
              );
              objQuestion.question = currentline;
              allQuestions.push(objQuestion);
            }
          }
        }
        allTopics.sort();
        for (var i = 0; i < allTopics.length; i++) {
          document.getElementById("topicsList").innerHTML +=
            '<option class="topicOption" value="' +
            i +
            '">' +
            allTopics[i] +
            "</option></li>";
          document.getElementById("topicsList02").innerHTML +=
            '<option class="topicOption" value="' +
            i +
            '">' +
            allTopics[i] +
            "</option></li>";
        }
        break;
      case "grammargamble":
        lines = gameRawContent.split("\n");
        for (var i = 0; i < lines.length; i++) {
          let objQuestion = {};
          if (lines[i].charAt(0) === ":") {
            currentline = lines[i].replace(/^(:+\s*)+/g, ""); // Get rid of starting colon and following space
            topic = currentline.replace(/(\r\n|\n|\r)/gm, "").trim(); // Save topic title
            allTopics.push(topic);
          } else {
            objQuestion.topic = topic;
            currentline = lines[i]; // Save questions content
            if (currentline !== "") {
              // Add images links for [] markers
              // currentline = currentline.replace(
              //   /\[(.*)\]/g,
              //   "<img class='img-rounded' src='./assets/data/img/$1' alt='?' />",
              // );
              currentline = currentline.replace(
                /\[(.*)\]/g,
                "<img class='img-rounded' src='../../data/img/$1' alt='?' />",
              );
              objQuestion.question = currentline;
              allQuestions.push(objQuestion);
            }
          }
        }
        allTopics.sort();
        for (var i = 0; i < allTopics.length; i++) {
          document.getElementById("topicsList").innerHTML +=
            '<option class="topicOption" value="' +
            i +
            '">' +
            allTopics[i] +
            "</option></li>";
          document.getElementById("topicsList02").innerHTML +=
            '<option class="topicOption" value="' +
            i +
            '">' +
            allTopics[i] +
            "</option></li>";
        }
        break;
      default:
        console.log("todo");
        break; //TODO Add default format
    }
  });
}

function drawTeams(nb = 2, toDisplay = true) {
  selectedPlayers = allPlayers.filter(
    (player) => player.team == selectedTeamName,
  );
  shuffle(selectedPlayers);
  let teamNb = 1;
  selectedPlayers.forEach(function (player) {
    player.gameTeam = teamNb;
    player.active = 1;
    if (player.isAbsent == 0) {
      if (teamNb !== nb) {
        teamNb++;
      } else {
        teamNb = 1;
      }
    }
  });
  isTeamsSelected = true;
  displayGroups(nb);
  if (toDisplay) {
    syncTeams();
  }
}

function showTeam(className) {
  unSelectAllPlayers();
  var selectPlayers = document.querySelectorAll(
    "li[data-team='" + className + "']",
  );
  selectPlayers.forEach(function (player) {
    player.classList.add("activePlayer");
  });
  document.querySelectorAll(".btn.selectedTeam").forEach(function (el) {
    el.classList.remove("selectedTeam");
  });
  event.target.classList.add("selectedTeam");
  selectedTeamName = className;
  isTeamsSelected = false;
}

function toggleAbsent(playerId) {
  let player = allPlayers.find((player) => player.id == playerId);
  let checkBox = document.getElementById("cb-" + playerId);
  let label = findLabelForControl(checkBox);
  if (isTeamsSelected) {
    var playerDiv = document.getElementById("pl-" + player.id);
  }
  if (player.isAbsent === 0) {
    player.isAbsent = 1;
    label.classList.add("strike");
  } else {
    player.isAbsent = 0;
    label.classList.remove("strike");
  }
  displayGroups();
}
function displayGroups(nb = 2) {
  if (gameTeamsDiv) {
    let allTeamNames = gameTeamsDiv.querySelectorAll(".teamName");
    allTeamNames.forEach(function (el, index) {
      if (index < nb) {
        el.classList.remove("hidden");
      } else {
        el.classList.add("hidden");
      }
    });
    let allTeamPlayersLists = gameTeamsDiv.querySelectorAll(".teamPlayersList");
    allTeamPlayersLists.forEach(function (el) {
      el.innerHTML = "";
    });
    absentPlayersDiv = document.getElementById("displayAbsentPlayers");
    absentPlayersDiv.innerHTML = "";
    for (var i = 0; i < selectedPlayers.length; i++) {
      if (selectedPlayers[i].isAbsent == 0) {
        let playerTeam = selectedPlayers[i].gameTeam;
        let playerTeamList = document.getElementById(
          "displayTeam0" + playerTeam + "Players",
        );
        playerTeamList.innerHTML +=
          '<li id="pl-' +
          selectedPlayers[i].id +
          '" onclick="actionMenu(event, ' +
          selectedPlayers[i].id +
          ');" data-playerId="' +
          selectedPlayers[i].id +
          '">' +
          selectedPlayers[i].name +
          "</li>";
      } else {
        absentPlayersDiv.innerHTML +=
          '<li id="pl-' +
          selectedPlayers[i].id +
          '" onclick="actionMenu(event, ' +
          selectedPlayers[i].id +
          ');" data-playerId="' +
          selectedPlayers[i].id +
          '" class="strike">' +
          selectedPlayers[i].name +
          "</li>";
      }
    }
    allTeamNames.forEach(function (el) {
      const teamList = el.querySelector("ul");
      sortList(teamList);
    });
  }
}

function syncTeams() {
  if (gameTeamsDiv) {
    const allShownTeams = gameTeamsDiv.querySelectorAll(
      ".teamName:not(.hidden)",
    );
    if (allShownTeams.length > 0) {
      allShownTeams.forEach(function (el, index) {
        let teamDiv = document.getElementById(
          "team0" + parseInt(index + 1) + "Players",
        );
        if (teamDiv != null) {
          teamDiv.innerHTML = el.querySelector("ul").innerHTML;
        }
      });
    }
  }
}

function unSelectAllPlayers() {
  var allPlayers = document.querySelectorAll("#playersList > li");
  allPlayers.forEach(function (item) {
    item.className = "teamPlayer";
  });
}

function pickPlayer(gameTeam, forget = false) {
  const teamPlayers = allPlayers.filter(
    (player) =>
      player.team == selectedTeamName &&
      player.gameTeam == gameTeam &&
      player.isAbsent == 0 &&
      player.active == 1,
  );
  let currentPlayer;
  if (teamPlayers.length === 1) {
    // All players have been selected once
    let teamPlayers = allPlayers.filter(
      (player) =>
        player.team == selectedTeamName &&
        player.gameTeam == gameTeam &&
        player.isAbsent == 0,
    );
    teamPlayers.forEach(function (player) {
      player.active = 1;
    });
  }
  if (teamPlayers.length > 1) {
    // Useful when no class is selected
    const random = Math.floor(Math.random() * teamPlayers.length);
    currentPlayer = teamPlayers[random];
    if (forget == false) {
      currentPlayer.active = 0;
    }
  } else {
    currentPlayer = teamPlayers[0];
  }
  return currentPlayer;
}

function selectTopic(idSender = "topicsList", idSync = "topicsList02") {
  // let e = document.getElementById('topicsList');
  let e = document.getElementById(idSender);
  let value = e.value;
  let text = e.options[e.selectedIndex].text;
  let index = e.selectedIndex;
  selectedQuestions = [];
  // document.getElementById('selectedTopic').innerHTML = text;
  document.getElementById(idSync).selectedIndex = e.selectedIndex;
  let selQuestions = allQuestions.filter((question) => question.topic == text);
  for (var i = 0; i < selQuestions.length; i++) {
    selectedQuestions.push(selQuestions[i]);
  }
  if (index !== 0) {
    isTopicSelected = true;
  } else {
    isTopicSelected = false;
  }
  document.getElementById("startButton").removeAttribute("disabled");
  if (idSender == "topicsList02") {
    mixBoard(true, false);
  }
  return selectedQuestions;
}

function sortByName(a, b) {
  let fa = a.name.toLowerCase(),
    fb = b.name.toLowerCase();
  if (fa < fb) {
    return -1;
  }
  if (fa > fb) {
    return 1;
  }
  return 0;
}

function sortList(list) {
  var list, i, switching, b, shouldSwitch;
  // list = document.getElementById("id01");
  switching = true;
  /* Make a loop that will continue until
  no switching has been done: */
  while (switching) {
    // Start by saying: no switching is done:
    switching = false;
    b = list.getElementsByTagName("LI");
    // Loop through all list items:
    for (i = 0; i < b.length - 1; i++) {
      // Start by saying there should be no switching:
      shouldSwitch = false;
      /* Check if the next item should
      switch place with the current item: */
      if (b[i].innerHTML.toLowerCase() > b[i + 1].innerHTML.toLowerCase()) {
        /* If next item is alphabetically lower than current item,
        mark as a switch and break the loop: */
        shouldSwitch = true;
        break;
      }
    }
    if (shouldSwitch) {
      /* If a switch has been marked, make the switch
      and mark the switch as done: */
      b[i].parentNode.insertBefore(b[i + 1], b[i]);
      switching = true;
    }
  }
}

function shuffle(values) {
  let index = values.length,
    randomIndex;
  // While there remain elements to shuffle.
  while (index != 0) {
    // Pick a remaining element.
    randomIndex = Math.floor(Math.random() * index);
    index--;
    // And swap it with the current element.
    [values[index], values[randomIndex]] = [values[randomIndex], values[index]];
  }
  return values;
}

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

function getOffset(el) {
  const rect = el.getBoundingClientRect();
  return {
    left: rect.left + window.scrollX,
    right: rect.right + window.scrollX,
    top: rect.top + window.scrollY,
    bottom: rect.bottom + window.scrollY,
  };
}

function positionElement(el, x, y = "", unit = "px") {
  el.style.position = "absolute";
  el.style.left = x + unit;
  if (y != "") {
    el.style.top = y + unit;
  }
}

function randomIntFromInterval(min, max) {
  // min and max included
  return Math.floor(Math.random() * (max - min + 1) + min);
}

function findLabelForControl(el) {
  var idVal = el.id;
  labels = document.getElementsByTagName("label");
  for (var i = 0; i < labels.length; i++) {
    if (labels[i].htmlFor == idVal) return labels[i];
  }
}

function hide(div, hideDarkOverlay = true, killTimer = false) {
  div.classList.add("hidden");
  if (hideDarkOverlay) {
    darkOverlay.classList.add("hidden");
  }
  if (killTimer && typeof myTimer !== "undefined") {
    clearInterval(myTimer);
  }
}

function show(div, showDarkOverlay = true, hideAllDialogs = true) {
  if (hideAllDialogs) {
    hideAll();
  }
  if (showDarkOverlay) {
    darkOverlay.classList.remove("hidden");
  }
  div.classList.remove("hidden");
}

function hideAll() {
  document.querySelectorAll(".lightbox, .darkOverlay").forEach(function (el) {
    hide(el);
  });
}

function toggle(div, hideAllDialogs = true) {
  if (div.classList.contains("hidden")) {
    show(div);
  } else {
    if (hideAllDialogs) {
      hideAll();
    } else {
      hide(div);
    }
  }
}

function displayTimer(timeLeft) {
  let minutes = parseInt(timeLeft / 60, 10);
  let seconds = parseInt(timeLeft % 60, 10);
  minutes = minutes < 10 ? "0" + minutes : minutes;
  seconds = seconds < 10 ? "0" + seconds : seconds;
  timerDiv.setAttribute("data-timeLeft", timeLeft);
  timerDiv.innerHTML = `${minutes}:${seconds}`;
  timerDiv.classList.remove("paused");
  timerDiv.classList.remove("hidden");
  hide(document.getElementById("setTimerDiv"));
  startTimer(timeLeft);
}

function startTimer(timeLeft, displayMinutes = true) {
  timeLeft -= 1; // Hack to avoid 1s delay when starting (feels better)
  myTimer = setInterval(() => {
    let minutes = parseInt(timeLeft / 60, 10);
    let seconds = parseInt(timeLeft % 60, 10);
    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;
    timerDiv.setAttribute("data-timeLeft", timeLeft);
    timerDiv.setAttribute("data-displayMinutes", displayMinutes);
    let callbackName = timerDiv.getAttribute("data-callbackName");
    if (displayMinutes && displayMinutes != "false") {
      timerDiv.innerHTML = `${minutes}:${seconds}`;
    } else {
      if (minutes > 0) {
        timerDiv.innerHTML = `${minutes}:${seconds}`;
      } else {
        timerDiv.innerHTML = `${seconds}`;
      }
    }
    if (timeLeft < 6) {
      audioTick.play();
    }
    if (timeLeft == 0) {
      clearInterval(myTimer);
      if (callbackName == "soccer") {
        removeTimer();
        endGame();
      }
      if (callbackName == "carrace") {
        stopTimer();
      }
    }
    timeLeft = timeLeft <= 0 ? 0 : timeLeft - 1;
  }, 1000);
}

function pauseResumeTimer() {
  let timeLeft = timerDiv.getAttribute("data-timeLeft");
  let displayMinutes = timerDiv.getAttribute("data-displayMinutes");
  if (timerDiv.classList.contains("paused")) {
    startTimer(timeLeft, displayMinutes);
  } else {
    clearInterval(myTimer);
  }
  timerDiv.classList.toggle("paused");
}

function removeTimer() {
  if (myTimer != "undefined") {
    clearInterval(myTimer);
  }
  timerDiv.innerHTML = "";
  hide(timerDiv);
  hideAll();
}

function loadSavedGames() {
  allSavedGames = localStorage.getItem("flgames");
  if (allSavedGames != null && document.title == "FLGames") {
    document.getElementById("footer").classList.add("blink");
    document.getElementById("savedGameButton").classList.remove("hidden");
    savedDiv = document.getElementById("loaded");
    savedDiv.innerHTML += allSavedGames;
    let savedGames = document.querySelectorAll(".savedGame");
    for (let i = 0; i < savedGames.length; i++) {
      let deleteButton = document.createElement("span");
      deleteButton.classList.add("deleteButton");
      deleteButton.innerHTML = "&#10006;";
      deleteButton.onclick = function () {
        if (confirm("Permanently delete this game ?")) {
          trashSavedGame();
        }
      };
      if (savedGames[i].querySelector("h4 > .badge") != null) {
        savedGames[i].querySelector("h4 > .badge").prepend(deleteButton);
      } else {
        savedGames[i].prepend(deleteButton);
      }
    }
  }
}

function emptySavedGames() {
  if (confirm("Permanently delete all saved games ?")) {
    localStorage.removeItem("flgames");
    document.getElementById("loaded").innerHTML = "";
    hide(savedGamesDiv);
    document.getElementById("footer").classList.remove("blink");
    document.getElementById("savedGameButton").classList.add("hidden");
  }
}

function trashSavedGame() {
  let parent = event.target.closest("div.savedGame");
  parent.remove();
  // savedGamesDiv.querySelectorAll(".deleteButton").forEach(function (el) {
  //   el.remove(); // Delete buttons because they are created at load time
  // });
  let toSave = document.getElementById("loaded").innerHTML;
  if (toSave == "") {
    // No more saved games
    localStorage.removeItem("flgames");
    hide(savedGamesDiv);
    document.getElementById("footer").classList.remove("blink");
    document.getElementById("savedGameButton").classList.add("hidden");
  } else {
    localStorage.setItem("flgames", toSave);
  }
}

function confirmAction(action, url, title, message) {
  dialogDiv.querySelector("h1").innerHTML = title;
  dialogDiv.querySelector("p").innerHTML = message;
  dialogDiv.setAttribute("action", action);
  dialogDiv.setAttribute("url", url);
  show(dialogDiv);
  return false;
}

function isConfirm(answer) {
  let action = dialogDiv.getAttribute("action");
  let url = dialogDiv.getAttribute("url");
  switch (action) {
    case "end":
      if (answer) {
        showFinalBoard();
      }
      break;
    case "trashSaved":
      trashSavedGame();
      break;
    case "go":
    default: // Go to URL
      if (answer) {
        window.location.href = url;
      }
      break;
  }
  hide(dialogDiv);
  return false;
}

function goHome() {
  let url = "";
  if (event.currentTarget.tagName == "A") {
    url = event.currentTarget.getAttribute("href");
  } else {
    url = event.currentTarget.getAttribute("data-href");
  }
  confirmAction("go", url, lang["quitGameTitle"], lang["quitGameSubtitle"]);
  return false;
}

function findGetParameter(parameterName) {
  var result = null,
    tmp = [];
  location.search
    .substr(1)
    .split("&")
    .forEach(function (item) {
      tmp = item.split("=");
      if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
    });
  return result;
}

async function includeJs(jsFilePath, id) {
  let js = document.createElement("script");
  js.type = "text/javascript";
  js.src = jsFilePath;
  js.async = false;
  js.defer = false;
  js.id = id;
  // document.head.appendChild(js);
  document.body.append(js);
}

async function loadScript(url, callback) {
  // adding the script element to the head as suggested before
  var head = document.getElementsByTagName("head")[0];
  var script = document.createElement("script");
  script.type = "text/javascript";
  script.src = url;
  // then bind the event to the callback function
  // there are several events for cross browser compatibility
  script.onreadystatechange = callback;
  script.onload = callback;
  // fire the loading
  head.appendChild(script);
}

function setCookie(cname, cvalue, exdays) {
  const d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  let expires = "expires=" + d.toUTCString();
  document.cookie =
    cname + "=" + cvalue + ";" + expires + ";path=/;Secure;SameSite=Strict";
}

function getCookie(cname) {
  let name = cname + "=";
  let ca = document.cookie.split(";");
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == " ") {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function removeFromLocalStorage() {
  if (allSavedGames != null) {
    localStorage.setItem("flgames", allSavedGames);
  } else {
    localStorage.removeItem("flgames");
  }
  document.getElementById("saveButton").classList.remove("hidden");
  document.getElementById("unSaveButton").classList.add("hidden");
}

function saveToLocalStorage() {
  let toSave = "";
  endDiv.querySelectorAll("#endDiv > .toSave").forEach(function (el) {
    let clonedEl = el.cloneNode(true);
    clonedEl.removeAttribute("data-i18n, data-i18nt");
    if (clonedEl.querySelector(".closeIcon") != null) {
      clonedEl.querySelector(".closeIcon").remove();
    }
    toSave += clonedEl.innerHTML;
  });
  toSave = '<div class="savedGame">' + toSave + "</div>";
  let alreadySaved = localStorage.getItem("flgames");
  if (alreadySaved != null) {
    alreadySaved += toSave;
    localStorage.setItem("flgames", alreadySaved);
  } else {
    localStorage.setItem("flgames", toSave);
  }
  document.getElementById("saveButton").classList.add("hidden");
  document.getElementById("unSaveButton").classList.remove("hidden");
}

function addEvent(obj, type, fn) {
  if (obj.addEventListener) {
    obj.addEventListener(type, fn, false);
  } else if (obj.attachEvent) {
    obj["e" + type + fn] = fn;
    obj[type + fn] = function () {
      obj["e" + type + fn](window.event);
    };
    obj.attachEvent("on" + type, obj[type + fn]);
  } else {
    obj["on" + type] = obj["e" + type + fn];
  }
}

function dragElement(elmnt, direction = "all", callback = null) {
  var pos1 = 0,
    pos2 = 0,
    pos3 = 0,
    pos4 = 0;
  if (document.getElementById(elmnt.id + "header")) {
    // if present, the header is where you move the DIV from:
    document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
  } else {
    // otherwise, move the DIV from anywhere inside the DIV:
    elmnt.onmousedown = dragMouseDown;
  }

  function dragMouseDown(e) {
    e = e || window.event;
    e.preventDefault();
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    switch (direction) {
      case "x":
        elmnt.style.left = elmnt.offsetLeft - pos1 + "px";
        break;
      case "y":
        elmnt.style.top = elmnt.offsetTop - pos2 + "px";
        break;
      case "all":
      default:
        elmnt.style.top = elmnt.offsetTop - pos2 + "px";
        elmnt.style.left = elmnt.offsetLeft - pos1 + "px";
        break;
    }
  }

  function closeDragElement() {
    // stop moving when mouse button is released:
    document.onmouseup = null;
    document.onmousemove = null;
    // Call ending function
    if (callback != "") {
      callback();
    }
  }
}

async function includeHTML(cb) {
  var z, i, elmnt, file, xhttp;
  z = document.querySelectorAll("[w3-include-html");
  z.forEach(function (elmnt) {
    file = elmnt.getAttribute("w3-include-html");
    if (file) {
      xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function () {
        if (this.readyState == 4) {
          if (this.status == 200) {
            elmnt.outerHTML = this.responseText;
          }
          if (this.status == 404) {
            elmnt.innerHTML = "Page not found.";
          }
          elmnt.removeAttribute("w3-include-html");
        }
      };
      xhttp.open("GET", file, true);
      xhttp.send();
    }
    // }
  });
  if (cb) {
    setTimeout(cb, 100);
  }
}
