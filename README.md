# FLGames

## Description

FLGames, ce sont des petits jeux éducatifs que j'utilise en classe très
rgulièrement. Chaque jeu apporte ses spécificités (plus lent, plus rapide, en
groupe, à l'écrit, à l'oral…). Ce sont des jeux de classe : on projète le jeu
au TBI, et on joue avec tout le monde. Ils n'ont pas d'intérêt à être utilisés
seul par un élève hors de la classe.

## Installation

> Désormais accessibles directement en ligne avec un contenu personnalisable
> via le stockage local du naivgateur : <https://flgames.demapage.fr>

Sinon, il suffit de cloner le dépôt puis d'adapter à son contenu (des fichiers
textes, principalement) :

- /data/myClasses-data.txt : les prénoms et classes des élèves.
- /data/nomdujeu-data.txt : les jeux de données (attention à respecter le
  format !)
- Si besoin : /data/img/ : les images à utiliser dans certains jeux de données.

> ATTENTION à permettre au navigateur d'accéder aux fichiers locaux !
> Sur Firefox : aller dans about:config et mettre le paramètre
> security.fileuri.strict_origin_policy à false

L'interface peut être mise en français ou en anglais en cliquant sur les
drapeaux en bas de la page d'accueil.

Les résultats sont enregistrés en local (car je les saisis ensuite dans mon Planet Alert
(<https://planetalert.tuxfamily.org>) pour que les élèves valident leurs
points).

Tout se passe en local. Le projet peut donc être stocké sur clé USB et on
accède à l'ensemble des jeux à partir d'un simple navigateur (testé uniquement
sous Firefox)

## Roadmap

La version sur la Forge des communs numériques possède 6 jeux
(décris rapidement ci-dessous).

Les jeux sont testés régulièrement en classe et j'ajuste quelques détails selon
les beosins. N'hésitez pas à faire part de bugs rencontrés pour qu'une solution
soit trouvée.

## Et… ?

Si vous regardez les fichiers, vous comprendrez vite que je suis prof
d'anglais, mais les jeux ont été testés avec succès en Primaire en Maths, et en
Français. En réalité, n'importe quel contenu peut être utilisé.

Bien que disponibles sur Interent, les jeux restent ancrés dans le réel de la
classe. Ils n'ont pas grand intérêt pour les élèves chez eux. Ce sont des
activités qui visent à motiver les élèves durant leur temps en classe et
l'interacion entre eux et avec l'enseignant⋅e est primordiale.

FLGames est distribué sous Licence Libre.

## Les jeux

Les règles plus détaillées figurent dans les jeux (icône en bas à droite).

### Tic-Tac-Toe (= le morpion)

Un simple jeu de morpion. 2 équipes (au hasard). Pour jouer dans sa case, il
faut donner la bonne réponse. Ce jeu permet de mettre en place du vocabulaire
ou une chose précise car il est plutôt lent. Les élèves ont le temps de
réfléchir. Pour accélerer un peu les choses, je fais un chrono de 5 secondes
variables avec la main en classe.

### Soccer (= le football)

2 équipes et un match de foot « endiablé ». 1 membre de chaque équipe est tiré
au sort, puis 1 question, et le ou la première qui donne la bonne réponse fera
avancer le ballon vers le camps adverse (1, 2 ou 3 passes au hasard, de
longueur variable). Ce jeu développe la rapidité : il faut mobiliser
rapidemment ses connaissances pour gagner ! Utile en calcul mental, par exemple
!

### Car Race (= la course de voitures)

Chaque groupe possède un véhicule qu'il fera avancer (de 1 à 20 au hasard) en
donnant une bonne réponse sur l'ardoise. L'ardoise change de main à chaque
question. À la fin du chrono, on lève. L'enseignant⋅e vérifie et valide les
véhicules. Bonus et malus sont au programme selon l'ambiance de classe. Ce jeu
vise à développer l'entraide et la capacité à s'organiser en groupe.

### Grammar Gamble (= le casino de grammaire)

Une phrase est projetée. On décide individuellement si elle est
grammaticalement juste ou fausse. On parie un nombre de dollars. On ajoute ou
en retranche sa mise selon le résultat :) Ce jeu entraîne à la relecture.

### The Alphabet Game (= le jeu de l'alphabet)

2 équipes s'opposent. Une personne choisit une lettre. Celle-ci correspond à la
première lettre de la réponse à la question qui sera lue par l'enseignant⋅e. La
lettre s'efface lorsqu'on clique sur le nom de la personne la plus rapide à
donner la réponse. Lorsqu'il n'y a plus de lettres, l'équipe ayant le plus
grand score l'emporte. Ce jeu fait travailler la compréhension orale et plaît
beaucoup aux élèves. On peut s'amuser à sélectionner un ou une joueuse contre
plusieurs membre de l'autre équipe. Un exemple de fichier « questions » se
trouve là : « /games/alphabetgame/assets/data/alphabet_game.pdf », mais le jeu
ne projète aucune question au tableau (il n'y a donc pas de fichier
data/alphabetgame-data.txt).

### Writing Fight (= le combat d'écriture)

La classe joue contre l'enseignant⋅e (qui ne fait pas grand chose :) ). Des
mots sont affichés (selon le thème choisi). Sur bouts de papiers, les élèves
écrivent des phrases à l'aide de ces aides. Tous ces papiers sont réunis dans
une boîte (un couvercle de ramette, par exemple). L'enseignant⋅e tire un papier
au hasatd et recopie exactement ce qui est écrit. Tout le monde se concerte
pour corriger et le point sera attribué à l'équipe (la classe) si la grammaire
est bonne ou, le cas contraire, à l'enseignant⋅e. Si la grammaire est bonne, un
bonus est possible pour une phrase recevable (qui n'est pas répétée, qui ajoute
une complexité supplémentaire, …). La fin du jeu dépend de la fin de l'heure.
Tous les papiers ne sont pas corrigés. À la fin, l'équipe se voit créditée d'un
bonus dans mon autre jeu Planet Alert ou d'un malus :)
