// Global variables
allSavedGames = localStorage.getItem("flgames");

const audioApplause = new Audio("../../assets/audio/applause.ogg");

let isTeamsSelected = false;
let isTopicSelected = false;
let isGameStarted = false;
let selectedTeamName = "no-class";
let selectedPlayer = [];
let nbTeams = 2;
let nbGroups = 2;
let currentPlayerDiv = document.getElementById("currentPlayerName");
let initCurrentPlayerDiv = currentPlayerDiv.cloneNode(true); // Hack to replay CSS animation
let scoreTeam01 = document.getElementById("scoreTeam01");
let scoreTeam02 = document.getElementById("scoreTeam02");
let logoTeam01 = document.getElementById("logoTeam01");
let logoTeam02 = document.getElementById("logoTeam02");

function actionMenu(event, playerId) {
  let player = allPlayers.find((player) => player.id == playerId);
  document.getElementById("actionSelectedPlayer").innerHTML = player.name;
  document
    .getElementById("actionSelectedPlayer")
    .setAttribute("data-playerId", player.id);
  document
    .getElementById("actionSelectedPlayer")
    .setAttribute("data-team", player.team);
  show(actionDiv, false, true);
}

function action(type) {
  let playerId = document
    .getElementById("actionSelectedPlayer")
    .getAttribute("data-playerId");
  let player = allPlayers.find((player) => player.id == playerId);
  const parentTeam01 = document.getElementById("displayTeam01Players");
  const parentTeam02 = document.getElementById("displayTeam02Players");
  let playerDiv = document.getElementById("pl-" + player.id);
  switch (type) {
    case "switchTeam":
      if (parentTeam01.contains(playerDiv)) {
        // console.log('Team 01 > Team 02');
        parentTeam02.appendChild(playerDiv);
        sortList(parentTeam02);
        player.gameTeam = 2;
      } else {
        // console.log('Team 02 > Team 01');
        parentTeam01.appendChild(playerDiv);
        sortList(parentTeam01);
        player.gameTeam = 1;
      }
      show(gameTeamsDiv);
      break;
    case "toggleAbsent":
      let checkBox = document.getElementById("cb-" + playerId);
      checkBox.click();
      show(gameTeamsDiv);
      break;
    default:
      break;
  }
  hide(actionDiv);
}

function startGame() {
  if (newGame()) {
    if (!isTeamsSelected) {
      drawTeams();
    }
    scoreTeam01.innerHTML = 0;
    scoreTeam02.innerHTML = 0;
    // Populate board
    if (selectedQuestions.length < 9) {
      alert("Not enough items in the selected topic !");
      return false;
    } else {
      mixBoard();
    }
    hide(configDiv);
    selectedPlayer = pickPlayer(1);
    display(selectedPlayer);
    isGameStarted = true;
  } else {
    hide(configDiv);
  }
}

function newGame() {
  if (isGameStarted) {
    return confirm(lang["newGame"]);
  } else {
    return true;
  }
}

function endGame() {
  audioApplause.play();
  showFinalBoard();
  return false;
}

function initLogo() {
  currentPlayerDiv.replaceWith(initCurrentPlayerDiv, currentPlayerDiv);
  if (document.getElementById("tempLogo01")) {
    document.getElementById("tempLogo01").remove();
  }
  if (document.getElementById("tempLogo02")) {
    document.getElementById("tempLogo02").remove();
  }
  scoreTeam01.classList.remove("blink");
  scoreTeam02.classList.remove("blink");
  logoTeam01.classList.remove("hidden", "blink");
  logoTeam02.classList.remove("hidden", "blink");
}

function display(player) {
  initLogo();
  currentPlayerDiv.classList.remove("hidden");
  currentPlayerDiv.innerHTML = player.name;
  currentPlayerDiv.classList.add("zoom");
  if (player.gameTeam == 1) {
    let tempLogo = logoTeam01.cloneNode(false);
    tempLogo.id = "tempLogo01";
    document.body.appendChild(tempLogo);
    tempLogo.classList.add("selected", "blink");
    logoTeam01.classList.add("hidden");
  } else {
    let tempLogo = logoTeam02.cloneNode(false);
    tempLogo.id = "tempLogo02";
    document.body.appendChild(tempLogo);
    tempLogo.classList.add("selected", "blink");
    logoTeam02.classList.add("hidden");
  }
}

function dropPiece() {
  // TODO Manage history to go back in case of a mistake
  let clickedCell = event.target.closest("td");
  // Disable click event (CSS transparent div) to prevent double click
  document.getElementById("backdrop").setAttribute("style", "display: block;");
  setTimeout(function () {
    document.getElementById("backdrop").setAttribute("style", "display: none;");
  }, 1500);
  var logo = sendToBoard(event);
  sleep(700).then(() => {
    let clone = logo.cloneNode(false);
    clone.classList.remove("hidden");
    clone.id = "";
    clone.classList.add("clone");
    clickedCell.innerHTML = "";
    clickedCell.setAttribute("data-owner", selectedPlayer.gameTeam);
    clickedCell.classList.remove("selectedCell1", "selectedCell2");
    clickedCell.classList.add("selectedCell" + selectedPlayer.gameTeam);
    clickedCell.appendChild(clone);
    let winningData = checkWinner();
    if (!winningData) {
      switchTeam();
    } else {
      validWin(winningData);
    }
  });
}

function displayWinningMove(move) {
  document
    .getElementById("cell-" + move.charAt(0))
    .classList.add("winningMove", "blink");
  document
    .getElementById("cell-" + move.charAt(1))
    .classList.add("winningMove", "blink");
  document
    .getElementById("cell-" + move.charAt(2))
    .classList.add("winningMove", "blink");
  sleep(2000).then(() => {
    document
      .getElementById("cell-" + move.charAt(0))
      .classList.remove("winningMove", "blink");
    document
      .getElementById("cell-" + move.charAt(1))
      .classList.remove("winningMove", "blink");
    document
      .getElementById("cell-" + move.charAt(2))
      .classList.remove("winningMove", "blink");
  });
  resetBoard();
}

function resetBoard() {
  let allCells = document.getElementsByTagName("TD");
  for (let i = 0; i < allCells.length; i++) {
    allCells[i].setAttribute("data-owner", "");
    allCells[i].classList.remove("selectedCell1", "selectedCell2");
  }
}

function validWin(winningData) {
  let score;
  audioApplause.play();
  displayWinningMove(winningData.move);
  if (winningData.gameTeam == 1) {
    logoTeam01.classList.add("blink");
    scoreTeam01.classList.add("blink");
    score = parseInt(scoreTeam01.innerHTML) + 1;
    scoreTeam01.innerHTML = score;
  } else {
    logoTeam02.classList.add("blink");
    scoreTeam02.classList.add("blink");
    score = parseInt(scoreTeam02.innerHTML) + 1;
    scoreTeam02.innerHTML = score;
  }
  if (score === 5) {
    // End game
    sleep(2500).then(() => {
      endGame();
    });
  } else {
    sleep(2500).then(() => {
      mixBoard();
      switchTeam();
    });
  }
}

function checkWinner() {
  let allCells = document.getElementsByTagName("TD");
  var board = [];
  for (let i = 0; i < allCells.length; i++) {
    board.push(allCells[i].getAttribute("data-owner"));
  }
  const winMap = [123, 456, 789, 147, 258, 369, 357, 159];
  // convert the board to array represening the filled cells, per player.
  // each array item is a string of only the cells (indices) filled by a player
  const moves = board.reduce(
    (players, v, i) => {
      if (v) players[v - 1] += i + 1;
      return players;
    },
    ["", ""],
  );
  // find & return the winning combination
  const winningMove = winMap.find((comb) =>
    moves.some(
      (
        m, // there are only 2 sets of moves, one for each player
      ) =>
        // break down the current combination to array and check if every item exists
        // also in the current set of moves. quit on first match.
        comb
          .toString()
          .split("")
          .every((c) => m.includes(c)),
    ),
  );
  return winningMove
    ? {
        // get the first number of the winning-move,
        // substract 1 from it, and use as index to find which
        // player played that move from the board Array
        gameTeam: board[winningMove.toString()[0] - 1],
        move: winningMove.toString(),
      }
    : false;
}

function switchTeam() {
  if (selectedPlayer != "") {
    if (selectedPlayer.gameTeam == 1) {
      selectedPlayer = pickPlayer(2);
    } else {
      selectedPlayer = pickPlayer(1);
    }
    display(selectedPlayer);
  }
}

function switchPlayer() {
  if (selectedPlayer != "") {
    if (selectedPlayer.gameTeam == 1) {
      selectedPlayer = pickPlayer(1);
    } else {
      selectedPlayer = pickPlayer(2);
    }
    display(selectedPlayer);
  }
}

function sendToBoard(e) {
  // Get mouse position
  let left = e.clientX - 50;
  let top = e.clientY - 50;
  if (selectedPlayer.gameTeam == 1) {
    var tempLogo = document.getElementById("tempLogo01");
    logo = document.getElementById("logoTeam01");
    tempLogo.classList.remove("blink");
    positionElement(tempLogo, left, top);
    setTimeout(function () {
      tempLogo.remove();
    }, 700);
    setTimeout(function () {
      logo.classList.remove("hidden");
    }, 1000);
  } else {
    var tempLogo = document.getElementById("tempLogo02");
    logo = document.getElementById("logoTeam02");
    tempLogo.classList.remove("blink");
    positionElement(tempLogo, left, top);
    setTimeout(function () {
      tempLogo.remove();
    }, 700);
    setTimeout(function () {
      logo.classList.remove("hidden");
    }, 1000);
  }
  return logo;
}

function initBoard() {}

function mixBoard(confirmMessage = false, initBoard = true) {
  if (confirmMessage == true) {
    if (confirm(lang["mixTopic"])) {
      if (isTeamsSelected) {
        let cell;
        shuffle(selectedQuestions);
        for (let i = 0; i < 9; i++) {
          if (initBoard == true) {
            cell = document.getElementById("cell-" + parseInt(i + 1));
          } else {
            cell = document.querySelector(
              "#cell-" +
                parseInt(i + 1) +
                ":not(.selectedCell1):not(.selectedCell2)",
            );
          }
          if (cell) {
            cell.setAttribute("data-content", selectedQuestions[i].question);
            cell.setAttribute("data-owner", "");
            cell.classList.remove("selectedCell1", "selectedCell2");
            cell.innerHTML = selectedQuestions[i].question;
          }
        }
      }
    }
  } else {
    if (isTeamsSelected) {
      shuffle(selectedQuestions);
      for (let i = 0; i < 9; i++) {
        let cell = document.getElementById("cell-" + parseInt(i + 1));
        cell.setAttribute("data-content", selectedQuestions[i].question);
        cell.setAttribute("data-owner", "");
        cell.innerHTML = selectedQuestions[i].question;
      }
    }
  }
}

function showFinalBoard() {
  initFinalBoard();
  let winningTeamListDiv = document.getElementById("winningTeamList");
  let absentPlayersDiv = document.getElementById("absentPlayersFinal");
  let finalScoreTeam01 = document.getElementById("scoreTeam01").textContent;
  let finalScoreTeam02 = document.getElementById("scoreTeam02").textContent;
  let winningPlayers, absentPlayers, isTie;
  document.getElementById("endTitle").innerHTML =
    lang["finalScoreTitle"] +
    " : " +
    finalScoreTeam01 +
    " - " +
    finalScoreTeam02;
  if (finalScoreTeam01 > finalScoreTeam02) {
    document.getElementById("endSubTitle").innerHTML =
      lang["congratulations01"];
    winningPlayers = allPlayers.filter(
      (player) =>
        player.team == selectedTeamName &&
        player.gameTeam == 1 &&
        player.isAbsent == 0,
    );
    isTie = false;
  } else if (finalScoreTeam02 > finalScoreTeam01) {
    document.getElementById("endSubTitle").innerHTML =
      lang["congratulations02"];
    winningPlayers = allPlayers.filter(
      (player) =>
        player.team == selectedTeamName &&
        player.gameTeam == 2 &&
        player.isAbsent == 0,
    );
    isTie = false;
  } else {
    document.getElementById("endSubTitle").innerHTML = lang["tie"];
    isTie = true;
  }
  document.getElementById("winningTeam").innerHTML = selectedTeamName;
  if (!isTie) {
    winningPlayers.forEach(function (player) {
      if (player.isScorer >= 1) {
        winningTeamListDiv.insertAdjacentHTML(
          "beforeend",
          "<li>" + player.name + ' <span class="goal">&#10036;</span></li>',
        );
      } else {
        winningTeamListDiv.insertAdjacentHTML(
          "beforeend",
          "<li>" + player.name + "</li>",
        );
      }
    });
  } else {
    winningTeamListDiv.insertAdjacentHTML(
      "beforeend",
      "<li>" + lang["nobody"] + "</li>",
    );
  }
  absentPlayers = allPlayers.filter(
    (player) => player.team == selectedTeamName && player.isAbsent == 1,
  );
  if (absentPlayers.length > 0) {
    absentPlayers.forEach(function (player) {
      absentPlayersDiv.insertAdjacentHTML(
        "beforeend",
        "<li>" + player.name + "</li>",
      );
    });
  } else {
    absentPlayersDiv.insertAdjacentHTML(
      "beforeend",
      "<li>" + lang["nobody"] + "</li>",
    );
  }
  toggle(endDiv);
  // Store in localStorage
  saveToLocalStorage();
}

function initFinalBoard() {
  hideAll();
  document.getElementById("winningTeamList").innerHTML = "";
  document.getElementById("endSubTitle").innerHTML = "";
  document
    .getElementById("displayAbsentPlayers")
    .querySelectorAll("li")
    .forEach(function (el) {
      el.remove();
    });
}
