langLocal = {
  mixTopic: "Mix topic",
  switchTeam: "Switch team",
  switchPlayer: "Switch player",
  help01:
    "The selected player places his or her team's logo in a cell by giving the correct answer.",
  help02:
    "The teacher can change team or switch player too adapt to the class.",
  help03: "The first team to get 5 points id the winner.",
  help04: "All questions are stored in 'assets/data/topics.txt' file.",
  help05: "Have fun learning with FLGames !",
};
