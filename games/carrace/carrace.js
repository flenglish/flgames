// Global variables
allSavedGames = localStorage.getItem("flgames");

const audioApplause = new Audio("./assets/audio/applause.ogg");
const audioKlaxon = new Audio("./assets/audio/klaxon.ogg");
const audioStart = new Audio("./assets/audio/car_start.ogg");
const audioMove = new Audio("./assets/audio/car_move.ogg");
const audioTick = new Audio("./assets/audio/tick.ogg");

const gameBoardDiv = document.getElementById("gameBoard");
const finishDiv = document.getElementById("finish");
const answerDiv = document.getElementById("questionAnswer");
let absentPlayersDiv = document.getElementById("absentPlayersFinal");

let isTeamsSelected = false;
let isTopicSelected = false;
let isGameStarted = false;
let selectedTeamName = "no-class";
let marginLeftHack = getOffset(document.getElementById("gameBoard")).left;
let finishLineX = getOffset(finishDiv).left - marginLeftHack;
let logoList = [
  "./assets/img/f1.png",
  "./assets/img/beetle_01.png",
  "./assets/img/bike.png",
  "./assets/img/2cv.png",
  "./assets/img/4x4.png",
  "./assets/img/car.png",
  "./assets/img/tractor_01.png",
  "./assets/img/motorbike_01.png",
  "./assets/img/truck.png",
  "./assets/img/scooter.png",
  "./assets/img/mini.png",
  "./assets/img/police_car.png",
  "./assets/img/monster_truck.png",
  "./assets/img/bus_02.png",
  "./assets/img/vintage_02.png",
];
let negatives = [
  "work",
  "breakdown",
  "traffic",
  "redlight",
  "flat",
  "fuel",
  "accident",
  "policeman",
];
let positives = ["speed", "downhill", "highway", "escort"];
let allPos = [];
let nbGroups = 6;

window.onload = (event) => {
  document.querySelectorAll(".runner").forEach(function (el) {
    // Click vehicle's lane to select it
    addEvent(el, "click", function (e) {
      el.classList.toggle("selected");
    });
  });
  dragElement(finishDiv, "x", movedFinish);
  document.querySelector(".btn.selected").click(); // 6 vehicles by default
};

function movedFinish() {
  finishLineX = getOffset(finishDiv).left - marginLeftHack;
}

function throwDice(nbTimes = 10) {
  document.getElementById("backdrop").setAttribute("style", "display: block;");
  setTimeout(function () {
    document.getElementById("backdrop").setAttribute("style", "display: none;");
  }, 2500);
  let allRunners = document.querySelectorAll(".runner.selected");
  if (allRunners.length > 0) {
    allRunners.forEach(function (el, index) {
      let dieMod = el.querySelector(".bonus > img");
      if (dieMod) {
        dieMod = dieMod.getAttribute("alt");
      }
      let dieVal;
      switch (dieMod) {
        // Malus
        case "breakdown":
          dieVal = randomIntFromInterval(0, 2);
          break;
        case "work":
          dieVal = randomIntFromInterval(1, 6);
          break;
        case "accident":
          dieVal = randomIntFromInterval(0, 5);
          break;
        case "fuel":
          dieVal = randomIntFromInterval(0, 4);
          break;
        case "traffic":
          dieVal = randomIntFromInterval(1, 8);
          break;
        case "redlight":
          dieVal = randomIntFromInterval(0, 2);
          break;
        case "flat":
          dieVal = randomIntFromInterval(0, 3);
          break;
        case "policeman":
          dieVal = 0;
          break;
        // Bonus
        case "speed":
          dieVal = randomIntFromInterval(18, 24);
          break;
        case "highway":
          dieVal = randomIntFromInterval(19, 25);
          break;
        case "downhill":
          dieVal = randomIntFromInterval(20, 25);
          break;
        case "escort":
          dieVal = randomIntFromInterval(24, 27);
          break;
        default:
          dieVal = randomIntFromInterval(1, 20);
          break;
      }
      let dieEl = el.querySelector(".die");
      dieEl.classList.remove("hidden");
      dieEl.textContent = dieVal;
      el.setAttribute("data-die", dieVal);
    });
    let rollDie = setTimeout(function () {
      if (nbTimes > 0) {
        nbTimes--;
        throwDice(nbTimes);
      } else {
        clearTimeout(rollDie);
        moveVehicle();
      }
    }, 200);
  }
}

function moveVehicle() {
  let allRunners = document.querySelectorAll(".runner.selected > .vehicle");
  shuffle(allRunners);
  let keepGoing = true;
  audioMove.play();
  allRunners.forEach(function (el) {
    if (keepGoing) {
      let dieVal = el.parentNode.getAttribute("data-die");
      let step = dieVal * 7;
      let newLeft = getOffset(el).left + step - marginLeftHack;
      let newRight = getOffset(el).right + step - marginLeftHack;
      if (newRight >= finishLineX) {
        // Winner
        positionElement(el, finishLineX);
        el.classList.add("blink", "winner");
        sleep(3500).then(endGame);
        keepGoing = false;
      } else {
        positionElement(el, newLeft);
      }
      sleep(2500).then(initRunners);
    }
  });
  sleep(2600).then(getPos);
}

function initRunners() {
  document.querySelectorAll(".runner").forEach(function (el) {
    el.classList.remove("selected");
    el.querySelector(".bonus").classList.add("hidden");
    el.querySelector(".bonus").classList.remove("positive", "negative");
    el.querySelector(".die").classList.add("hidden");
  });
}

function setBonus() {
  let allRunners = document.querySelectorAll(".runner.selected > .bonus");
  allRunners.forEach(function (el) {
    shuffle(positives);
    el.innerHTML =
      '<img src="./assets/img/' +
      positives[0] +
      '.png" alt="' +
      positives[0] +
      '" />';
    el.classList.remove("hidden", "positive", "negative");
    el.classList.add("positive");
    el.parentElement.classList.remove("selected");
  });
}

function setMalus() {
  let allRunners = document.querySelectorAll(".runner.selected > .bonus");
  allRunners.forEach(function (el) {
    shuffle(negatives);
    el.innerHTML =
      '<img src="./assets/img/' +
      negatives[0] +
      '.png" alt="' +
      negatives[0] +
      '" />';
    el.classList.remove("hidden", "positive", "negative");
    el.classList.add("negative");
    el.parentElement.classList.remove("selected");
  });
}

function removeBonus(el) {
  event.stopPropagation();
  el.classList.remove("positive", "negative");
  el.classList.add("hidden");
}

function actionMenu(event, playerId) {
  let player = allPlayers.find((player) => player.id == playerId);
  document.getElementById("actionSelectedPlayer").innerHTML = player.name;
  document
    .getElementById("actionSelectedPlayer")
    .setAttribute("data-playerId", player.id);
  document
    .getElementById("actionSelectedPlayer")
    .setAttribute("data-team", player.team);
  show(actionDiv, false, true);
}

function action(type, sender = null) {
  let playerId = document
    .getElementById("actionSelectedPlayer")
    .getAttribute("data-playerId");
  switch (type) {
    case "toggleAbsent":
      let checkBox = document.getElementById("cb-" + playerId);
      checkBox.click();
      break;
    default:
      break;
  }
  hide(actionDiv);
  if (sender != null) {
    show(sender);
  }
}
function startGame() {
  if (newGame()) {
    if (!isTeamsSelected) {
      drawTeams(nbGroups, false);
    }
    hide(configDiv);
    isGameStarted = true;
  } else {
    hide(configDiv);
  }
}

function setGroups(nb) {
  document.querySelectorAll(".btn.selected").forEach(function (el) {
    el.classList.remove("selected");
  });
  event.target.classList.toggle("selected");
  document.querySelectorAll(".runner").forEach(function (el, index) {
    if (index < nb) {
      el.classList.remove("hidden");
      el.querySelector(".vehicle").setAttribute("src", logoList[index]);
    } else {
      el.classList.add("hidden");
    }
  });
  nbGroups = nb;
  if (isTeamsSelected) {
    drawTeams(nbGroups, false);
  }
}

function mixVehicles() {
  shuffle(logoList);
  document.querySelectorAll(".vehicle").forEach(function (el, index) {
    el.setAttribute("src", logoList[index]);
  });
}

function newGame() {
  if (isGameStarted) {
    return confirm(lang["newGame"]);
  } else {
    return true;
  }
}

function endGame() {
  audioApplause.play();
  showFinalBoard();
  return false;
}

function initLogo() {
  currentPlayerDiv.replaceWith(initCurrentPlayerDiv, currentPlayerDiv);
  if (document.getElementById("tempLogo01")) {
    document.getElementById("tempLogo01").remove();
  }
  if (document.getElementById("tempLogo02")) {
    document.getElementById("tempLogo02").remove();
  }
  scoreTeam01.classList.remove("blink");
  scoreTeam02.classList.remove("blink");
  logoTeam01.classList.remove("hidden", "blink");
  logoTeam02.classList.remove("hidden", "blink");
}

function display(player) {
  initLogo();
  currentPlayerDiv.classList.remove("hidden");
  currentPlayerDiv.innerHTML = player.name;
  currentPlayerDiv.classList.add("zoom");
  if (player.gameTeam == 1) {
    let tempLogo = logoTeam01.cloneNode(false);
    tempLogo.id = "tempLogo01";
    document.body.appendChild(tempLogo);
    tempLogo.classList.add("selected", "blink");
    logoTeam01.classList.add("hidden");
  } else {
    let tempLogo = logoTeam02.cloneNode(false);
    tempLogo.id = "tempLogo02";
    document.body.appendChild(tempLogo);
    tempLogo.classList.add("selected", "blink");
    logoTeam02.classList.add("hidden");
  }
}

function drawQuestion() {
  if (isTopicSelected) {
    initQuestion();
    let currentQuestion = document.getElementById("question");
    // Init question
    let question = false;
    let questionAnswer = false;
    let questionTimer = false;
    let showAnswer = false;
    let stopButton = false;
    let questionParts = [];
    // Pick a random question
    const random = Math.floor(Math.random() * selectedQuestions.length);
    let randQuestion = selectedQuestions[random].question;
    // Parse selected question for timer and answer
    questionParts = randQuestion.split("::");
    question = questionParts[0].trim();
    if (questionParts[1]) {
      questionAnswer = questionParts[1].trim();
    } else {
      questionAnswer = "---";
    }
    answerDiv.innerHTML = questionAnswer;
    if (questionParts[2]) {
      questionTimer = questionParts[2].trim();
    } else {
      questionTimer = 60; // Default : 60 seconds
    }
    timerDiv.classList.remove("hidden");
    timerDiv.innerHTML = questionTimer;
    timerDiv.classList.add("blink");
    show(questionDiv);
    sleep(2000)
      .then(() => {
        launchTimer();
      })
      .then(() => {
        currentQuestion.innerHTML = question;
      });
  }
}

function launchTimer() {
  let seconds = timerDiv.textContent;
  timerDiv.classList.remove("blink");
  timerDiv.classList.add("started");
  startTimer(seconds, false);
}

function stopTimer() {
  audioKlaxon.play();
  clearInterval(myTimer);
  timerDiv.textContent = 0;
  timerDiv.classList.add("blink");
  setTimeout(() => {
    timerDiv.classList.add("hidden");
    timerDiv.classList.remove("blink", "started");
  }, 1000);
}

function initQuestion() {
  hideAll();
  audioStart.play();
  document.getElementById("question").innerHTML = "";
  timerDiv.classList.add("hidden");
  answerDiv.classList.add("hidden");
  answerDiv.innerHTML = "";
}

function toggleAnswer() {
  if (answerDiv.classList.contains("hidden")) {
    answerDiv.classList.remove("hidden");
  } else {
    answerDiv.classList.add("hidden");
  }
}

function validWin(winningData) {
  let score;
  audioApplause.play();
  displayWinningMove(winningData.move);
  if (winningData.gameTeam == 1) {
    logoTeam01.classList.add("blink");
    scoreTeam01.classList.add("blink");
    score = parseInt(scoreTeam01.innerHTML + 1);
    scoreTeam01.innerHTML = score;
  } else {
    logoTeam02.classList.add("blink");
    scoreTeam02.classList.add("blink");
    score = parseInt(scoreTeam02.innerHTML + 1);
    scoreTeam02.innerHTML = score;
  }
  if (score === 5) {
    // End game
    sleep(2500).then(() => {
      endGame();
    });
  } else {
    sleep(2500).then(() => {
      mixBoard();
      switchTeam();
    });
  }
}

function getPos() {
  allPos = [];
  document.querySelectorAll(".runner > .vehicle").forEach(function (el) {
    if (el.parentNode.classList.contains("hidden") === false) {
      let left = getOffset(el).left;
      let groupName = el.getAttribute("data-group");
      let objPos = {
        groupName: groupName,
        left: parseInt(left),
      };
      allPos.push(objPos);
    }
  });
  allPos.sort((a, b) => b.left - a.left);
  allPos.sort();
  allPos.forEach(function (el, index) {
    el.pos = index + 1;
    let vehicle = document.querySelector(
      '.vehicle[data-group="' + el.groupName + '"]',
    );
    vehicle.setAttribute("data-pos", el.pos);
  });
  // console.log(allPos);
}

function setRunnerBadge(el, showGroup = false) {
  if (el.parentNode.classList.contains("hidden") === false) {
    let tempEl = el.cloneNode(false);
    let groupName = tempEl.getAttribute("data-group");
    tempEl.classList.remove("blink");
    tempEl.style.position = "relative";
    tempEl.style.left = "0px";
    tempEl.style.height = "30px";
    let newLi = document.createElement("li");
    let newSpan = document.createElement("span");
    // DONE Show percentage accopplished
    // let percentageDone = Math.round(getOffset(el).left*100/finishLineX);
    // if (percentageDone > 100) { percentageDone = 100; }
    // newSpan.innerText = ' [' + lang['group'] +  ' ' + groupName + '] - ' + percentageDone + '% accomplished';
    // DONE Show group details
    // let groupPlayersArray = [];
    // let groupPlayersList = '';
    // let groupPlayers = selectedPlayers.filter((player) => player.gameTeam == groupName && player.isAbsent == 0);
    // for (let i=0; i<groupPlayers.length; i++) {
    //   groupPlayersArray.push(groupPlayers[i].name);
    // }
    // groupPlayersList = groupPlayersArray.join(', ');
    // newSpan.innerHTML = ' [' + lang['group'] +  ' ' + groupName + ' : ' + groupPlayersList + ']';
    newSpan.innerHTML = " [" + lang["group"] + " " + groupName + " ]";
    let newBadge = document.createElement("span");
    newBadge.classList.add("badge");
    newBadge.innerText = el.getAttribute("data-pos");
    newLi.appendChild(newBadge);
    newLi.appendChild(tempEl);
    newLi.appendChild(newSpan);
    document.getElementById("winningTeamList").appendChild(newLi);
  }
}

function showFinalBoard() {
  initFinalBoard();
  let winningTeamListDiv = document.getElementById("winningTeamList");
  absentPlayersDiv = document.getElementById("absentPlayersFinal");
  let winningPlayers, absentPlayers, isTie;
  document.getElementById("endTitle").innerHTML = lang["finalScoreTitle"];
  document.getElementById("endSubTitle").innerHTML = lang["finalScoreSubtitle"];
  document.getElementById("winningTeam").innerHTML = selectedTeamName;
  allPos.forEach(function (allPos) {
    let vehicle = document.querySelector(
      '.vehicle[data-group="' + allPos.groupName + '"]',
    );
    setRunnerBadge(vehicle);
  });
  absentPlayers = allPlayers.filter(
    (player) => player.team == selectedTeamName && player.isAbsent == 1,
  );
  if (absentPlayers.length > 0) {
    absentPlayers.forEach(function (player) {
      absentPlayersDiv.insertAdjacentHTML(
        "beforeend",
        "<li>" + player.name + "</li>",
      );
    });
  } else {
    absentPlayersDiv.insertAdjacentHTML(
      "beforeend",
      "<li>" + lang["nobody"] + "</li>",
    );
  }
  toggle(endDiv);
  // Store in localStorage
  saveToLocalStorage();
  // toSave = '';
  // endDiv.querySelectorAll('#endDiv > .toSave').forEach( function (el) { toSave += el.innerHTML; });
  // toSave = '<div class="savedGame">' + toSave + '</div>';
  // let alreadySaved = localStorage.getItem('flgames');
  // if (alreadySaved != null) {
  //   toSave = alreadySaved + toSave;
  // }
  // localStorage.setItem('flgames', toSave);
}

function initFinalBoard() {
  hideAll();
  document.getElementById("winningTeamList").innerHTML = "";
  document.getElementById("endSubTitle").innerHTML = "";
  absentPlayersDiv.querySelectorAll("li").forEach(function (el) {
    el.remove();
  });
}
