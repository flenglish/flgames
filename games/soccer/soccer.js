// Global variables
allSavedGames = localStorage.getItem("flgames");

const audioReferee = new Audio("./assets/audio/referee.ogg");
const audioCheer = new Audio("./assets/audio/cheer.ogg");
const audioCheer02 = new Audio("./assets/audio/cheer02.ogg");
const audioTick = new Audio("./assets/audio/tick.ogg");

const questionDiv = document.getElementById("questionDiv");
const receiverDiv = document.getElementById("receiver");
const setTimerDiv = document.getElementById("setTimerDiv");

let isTeamsSelected = false;
let isTopicSelected = false;
let isGameStarted = false;
let selectedTeamName = "no-class";
let selectedTeam01Player = [];
let selectedTeam02Player = [];
let nbGroups = 2;
let scoreTeam01 = document.getElementById("scoreTeam01");
let scoreTeam02 = document.getElementById("scoreTeam02");
let scorer = [];
let myTimer;
let ball = document.getElementById("ball");
let field = document.getElementById("gameBoard");

window.onload = function () {
  allPlayers.forEach(function (player) {
    player.isYellowCard = 0;
    player.isRedCard = 0;
    player.isScorer = 0;
  });
};

function startGame() {
  if (newGame()) {
    if (!isTeamsSelected && selectedTeamName != "no-class") {
      drawTeams();
    }
    // displayTeams();
    scoreTeam01.innerHTML = 0;
    scoreTeam02.innerHTML = 0;
    positionElement(ball, 48.6, 51, "%");
    hide(configDiv);
    isGameStarted = true;
  } else {
    hide(configDiv);
  }
}

function newGame() {
  if (isGameStarted) {
    return confirm(lang["newGame"]);
  } else {
    return true;
  }
}

function endGame() {
  audioReferee.play();
  confirmAction("end", "", lang["endGameTitle"], lang["endGameSubtitle"]);
  return false;
}

function displayTeams() {
  initTeams();
  absentPlayers = allPlayers.filter(
    (player) => player.team == selectedTeamName && player.isAbsent == 1,
  );
  selectedPlayers.forEach(function (player) {
    if (player.isAbsent == 0) {
      document.getElementById(
        "team0" + player.gameTeam + "Players",
      ).innerHTML +=
        '<li id="pl-' +
        player.id +
        '" onclick="actionMenu(' +
        player.id +
        '); show(actionDiv);">' +
        player.name +
        "</li>";
    } else {
      document.getElementById(
        "team0" + player.gameTeam + "Players",
      ).innerHTML +=
        '<li class="hidden" id="pl-' +
        player.id +
        '" onclick="actionMenu(' +
        player.id +
        '); show(actionDiv);">' +
        player.name +
        "</li>";
    }
  });
  const parentTeam01 = document.getElementById("team01Players");
  sortList(parentTeam01);
  const parentTeam02 = document.getElementById("team02Players");
  sortList(parentTeam02);
}

function drawQuestion() {
  if (isTopicSelected) {
    initQuestion();
    let currentQuestion = document.getElementById("question");
    // Pick a random player
    if (isTeamsSelected) {
      selectedTeam01Player = pickPlayer(1);
    } else {
      selectedTeam01Player.name = lang["team01"];
    }
    document.getElementById("selectedTeam01Player").innerHTML =
      selectedTeam01Player.name;
    // Pick a random player
    if (isTeamsSelected) {
      selectedTeam02Player = pickPlayer(2);
    } else {
      selectedTeam02Player.name = lang["team02"];
    }
    document.getElementById("selectedTeam02Player").innerHTML =
      selectedTeam02Player.name;
    // Pick a random question
    const random = Math.floor(Math.random() * selectedQuestions.length);
    setTimeout(function () {
      document.querySelectorAll(".currentPlayer").forEach(function (div) {
        div.classList.add("started");
      });
    }, 1500);
    setTimeout(function () {
      currentQuestion.innerHTML = selectedQuestions[random].question;
    }, 3200);
    show(questionDiv);
  } else {
    console.log("no topic selected !");
  }
}

function initQuestion() {
  hideAll();
  audioReferee.play();
  document.getElementById("question").innerHTML = "";
  document.querySelectorAll(".currentPlayer").forEach(function (div) {
    div.classList.remove("started");
  });
}

function initTeams() {
  let list01 = document.getElementById("team01Players");
  let list02 = document.getElementById("team02Players");
  let absentList = document.getElementById("absentPlayers");
  list01.innerHTML = "";
  list02.innerHTML = "";
  absentList.innerHTML = "";
}

function actionMenu(event, playerId) {
  let player = allPlayers.find((player) => player.id == playerId);
  document.getElementById("actionSelectedPlayer").innerHTML = player.name;
  document
    .getElementById("actionSelectedPlayer")
    .setAttribute("data-playerId", player.id);
  document
    .getElementById("actionSelectedPlayer")
    .setAttribute("data-team", player.team);
  show(actionDiv, false, true);
}

function action(type, sender = null) {
  let playerId = document
    .getElementById("actionSelectedPlayer")
    .getAttribute("data-playerId");
  let player = allPlayers.find((player) => player.id == playerId);
  const parentTeam01 = document.getElementById("team01Players");
  const parentTeam02 = document.getElementById("team02Players");
  let playerDiv = document.getElementById("pl-" + player.id);
  let card;
  switch (type) {
    case "yellowCard":
      if (player.gameTeam === 1) {
        passBall("team02");
      } else {
        passBall("team01");
      }
      card = document.createElement("span");
      card.classList.add("yellowCard");
      playerDiv.append(card);
      player.isYellowCard = 1;
      break;
    case "redCard":
      if (player.gameTeam === 1) {
        passBall("team02");
      } else {
        passBall("team01");
      }
      card = document.createElement("span");
      card.classList.add("redCard");
      playerDiv.append(card);
      player.active = 0;
      player.isRedCard = 1;
      break;
    case "switchTeam":
      if (parentTeam01.contains(playerDiv)) {
        // console.log('Team 01 > Team 02');
        parentTeam02.appendChild(playerDiv);
        sortList(parentTeam02);
        player.gameTeam = 2;
      } else {
        // console.log('Team 02 > Team 01');
        parentTeam01.appendChild(playerDiv);
        sortList(parentTeam01);
        player.gameTeam = 1;
      }
      break;
    case "toggleAbsent":
      let checkBox = document.getElementById("cb-" + playerId);
      checkBox.click();
      break;
    default:
      break;
  }
  hide(actionDiv);
  if (sender != null) {
    show(sender);
  }
}

async function passBall(team) {
  hide(questionDiv);
  audioCheer.play();
  let nbPasses = randomIntFromInterval(1, 3); // Random 1 to 3 passes
  let receiver;
  for (var i = 0; i < nbPasses; i++) {
    // Ball position
    let left = getOffset(ball).left;
    let top = getOffset(ball).top;
    let randX;
    // Random pass within filed coordinates
    let randY = randomIntFromInterval(
      getOffset(field).top + 40,
      getOffset(field).bottom - 40,
    );
    if (team === "team01") {
      // Pass for team01
      randX = randomIntFromInterval(10, 70);
      if (
        left + randX > getOffset(field).right ||
        left + randX > getOffset(field).right - 100
      ) {
        // Goal for team 01
        if (i == 0) {
          receiver = selectedTeam01Player;
        }
        scorer = receiver;
        scorer.isScorer += 1;
        animShoot("team01");
        break;
      }
      receiver = pickPlayer(1, true);
    } else {
      // Pass for team 02
      randX = randomIntFromInterval(-10, -70);
      if (
        left + randX < getOffset(field).left ||
        left + randX < getOffset(field).left + 100
      ) {
        // Goal for team 02
        if (i == 0) {
          receiver = selectedTeam02Player;
        }
        scorer = receiver;
        scorer.isScorer += 1;
        animShoot("team02");
        break;
      }
      receiver = pickPlayer(2, true);
    }
    if (selectedTeamName != "no-class") {
      receiverDiv.innerHTML = receiver.name;
    }
    if (randY > top) {
      // Position receiverDiv
      positionElement(receiverDiv, left + randX - 50, randY + 40);
    } else {
      positionElement(receiverDiv, left + randX - 50, randY - 40);
    }
    if (i % 2 == 0) {
      // Ball animation
      ball.classList.remove("pass02");
      ball.classList.add("pass01");
    } else {
      ball.classList.remove("pass01");
      ball.classList.add("pass02");
    }
    if (selectedTeamName != "no-class") {
      receiverDiv.classList.remove("hidden");
    }
    positionElement(ball, left + randX, randY);
    await sleep(1000)
      .then(() => {
        ball.classList.remove("pass01", "pass02");
      })
      .then(() => {
        sleep(2000).then(() => {
          hide(receiverDiv);
        });
      });
  }
}

async function animShoot(team) {
  audioCheer02.play();
  let randX, randY;
  switch (team) {
    case "team01":
      randX = 78;
      randY = 53.5;
      break;
    case "team02":
      randX = 18;
      randY = 53.5;
      break;
  }
  ball.classList.add("shoot");
  positionElement(ball, randX, randY, "%");
  await sleep(500).then(() => {
    ball.classList.remove("shoot");
    score(team);
  });
}

function score(team) {
  let scoreDiv;
  switch (team) {
    case "team01": // +1 for team 01
      scoreDiv = document.getElementById("scoreTeam01");
      scorersDiv = document.getElementById("scorersTeam01");
      break;
    case "team02": // +1 for team 02
      scoreDiv = document.getElementById("scoreTeam02");
      scorersDiv = document.getElementById("scorersTeam02");
      break;
  }
  scoreDiv.innerHTML = parseInt(scoreDiv.innerHTML) + 1;
  scorersDiv.innerHTML += '<span class="scorer">' + scorer.name + "</span>";
  scoreDiv.classList.add("blink");
  document.getElementById(team).classList.add("blink");
  sleep(2000).then(() => {
    ball.classList.remove("blink");
  });
  positionElement(ball, 48.5, 53.5, "%");
  ball.classList.add("blink");
  hide(receiverDiv);
  sleep(3000).then(() => {
    ball.classList.remove("blink");
    scoreDiv.classList.remove("blink");
    document.getElementById(team).classList.remove("blink");
  });
}

function showFinalBoard() {
  initBoard();
  let winningTeamListDiv = document.getElementById("winningTeamList");
  let absentPlayersDiv = document.getElementById("absentPlayers");
  let foulPlayersDiv = document.getElementById("foulPlayers");
  let finalScoreTeam01 = document.getElementById("scoreTeam01").textContent;
  let finalScoreTeam02 = document.getElementById("scoreTeam02").textContent;
  let winningPlayers, absentPlayers, isTie;
  document.getElementById("endTitle").innerHTML =
    lang["finalScoreTitle"] + finalScoreTeam01 + " - " + finalScoreTeam02;
  if (finalScoreTeam01 > finalScoreTeam02) {
    document.getElementById("endSubTitle").innerHTML = lang["congrat01"];
    winningPlayers = allPlayers.filter(
      (player) =>
        player.team == selectedTeamName &&
        player.gameTeam == 1 &&
        player.isAbsent == 0,
    );
    isTie = false;
  } else if (finalScoreTeam02 > finalScoreTeam01) {
    document.getElementById("endSubTitle").innerHTML = lang["congrat02"];
    winningPlayers = allPlayers.filter(
      (player) =>
        player.team == selectedTeamName &&
        player.gameTeam == 2 &&
        player.isAbsent == 0,
    );
    isTie = false;
  } else {
    document.getElementById("endSubTitle").innerHTML = lang["tie"];
    isTie = true;
  }
  document.getElementById("winningTeam").innerHTML = selectedTeamName;
  if (!isTie) {
    winningPlayers.forEach(function (player) {
      if (player.isScorer >= 1) {
        winningTeamListDiv.insertAdjacentHTML(
          "beforeend",
          "<li>" + player.name + ' <span class="goal">&#10036;</span></li>',
        );
      } else {
        winningTeamListDiv.insertAdjacentHTML(
          "beforeend",
          "<li>" + player.name + "</li>",
        );
      }
    });
  } else {
    winningTeamListDiv.insertAdjacentHTML(
      "beforeend",
      "<li>" + lang["nobody"] + "</li>",
    );
  }
  absentPlayers = allPlayers.filter(
    (player) => player.team == selectedTeamName && player.isAbsent == 1,
  );
  if (absentPlayers.length > 0) {
    absentPlayers.forEach(function (player) {
      absentPlayersDiv.insertAdjacentHTML(
        "beforeend",
        "<li>" + player.name + "</li>",
      );
    });
  } else {
    absentPlayersDiv.insertAdjacentHTML(
      "beforeend",
      "<li>" + lang["nobody"] + "</li>",
    );
  }
  foulPlayers = allPlayers.filter(
    (player) =>
      player.team == selectedTeamName &&
      (player.isYellowCard == 1 || player.isRedCard == 1),
  );
  if (foulPlayers.length > 0) {
    foulPlayers.forEach(function (player) {
      if (player.isYellowCard == 1) {
        foulPlayersDiv.insertAdjacentHTML(
          "beforeend",
          "<li>" + player.name + '<span class="yellowCard"></span></li>',
        );
      }
      if (player.isRedCard == 1) {
        foulPlayersDiv.insertAdjacentHTML(
          "beforeend",
          "<li>" + player.name + '<span class="redCard"></span></li>',
        );
      }
    });
  } else {
    foulPlayersDiv.insertAdjacentHTML(
      "beforeend",
      "<li>" + lang["nobody"] + "</li>",
    );
  }
  toggle(endDiv);
  // Store in localStorage ?
  saveToLocalStorage();
  // let toSave = '';
  // endDiv.querySelectorAll('#endDiv > .toSave').forEach( function (el) { toSave += el.innerHTML; });
  // toSave = '<div class="savedGame">' + toSave + '</div>';
  // let alreadySaved = localStorage.getItem('flgames');
  // if (alreadySaved != null) {
  //   toSave = alreadySaved + toSave;
  // }
  // localStorage.setItem('flgames', toSave);
}
function initBoard() {
  hideAll();
  audioCheer02.play();
  document.getElementById("winningTeamList").innerHTML = "";
  document.getElementById("endSubTitle").innerHTML = "";
  document
    .getElementById("absentPlayers")
    .querySelectorAll("li")
    .forEach(function (el) {
      el.remove();
    });
  document
    .getElementById("foulPlayers")
    .querySelectorAll("li")
    .forEach(function (el) {
      el.remove();
    });
}
