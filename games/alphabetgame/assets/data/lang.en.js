var langLocal = {
  finalScoreTitle: "Final result : ",
  finalScoreSubtitle: "",
  winningTeam: "Winning team",
  allScorers: "Scorers",
  congrat01: "Congratulations to team 01 !",
  congrat02: "Congratulations to team 02 !",
  tie: "It's a tie…",
  resetBoard: "Reset Board",
  selectAll: "All",
  selectAllPlayers: "All players",
  select1Players: "1 player",
  select2Players: "2 players",
  select3Players: "3 players",
  help01:
    "Pick players from each team (use the buttons or click on the names).",
  help02: "Have 1 player select a letter from the board. Click on it.",
  help03:
    "Ask a question from the list (on a printed sheet of paper) of the type : What (letter) is… ?",
  help04:
    "Click on the ✓ sign next to the fastest player giving the correct answer.",
  help05: "The letter will disappear.",
  help06: "Pick other players, another letter… until the board is empty.",
  help09: "All questions are stored in 'assets/data/topics.txt' file.",
  help10: "Have fun learning with FLGames !",
};
