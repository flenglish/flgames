var langLocal = {
  finalScoreTitle: "Résultat final : ",
  finalScoreSubtitle: "",
  winningTeam: "Équipe gagnante",
  allScorers: "Élèves ayant marqué des points",
  congrat01: "Félicitations à l'équipe 01 !",
  congrat02: "Félicitations à l'équipe 02 !",
  tie: "Match nul…",
  resetBoard: "Réinitialiser le plateau de jeu",
  selectAll: "Tous",
  selectAllPlayers: "Tous",
  select1Players: "1 joueur",
  select2Players: "2 joueurs",
  select3Players: "3 joueurs",
  help01:
    "Tiez au sort des membres de chaque équipe (utilisez les boutons ou cliquez sur les noms)",
  help02:
    "Une personne choisit une lettre. On la sélectionne en cliquant dessus.",
  help03:
    "Posez une question depuis la liste (du genre : Quel mot commençant par (la lettre)… ?",
  help04:
    "Cliquez sur le signe ✓ à côté de la personne la plus rapide à donner la bonne réponse.",
  help05: "La lettre disparaît.",
  help06:
    "Tirez d'autres personnes, choisissez une autre lettre… jusqu'à ce que le plateau de jeu soit vide.",
  help09:
    "Toutes les questions sont stockées dans le fichier 'assets/data/topics.txt' file.",
  help10: "Jouez bien avec FLGames !",
};
