// Global variables
allSavedGames = localStorage.getItem("flgames");

const audioApplause = new Audio("./assets/audio/applause.ogg");

const gameBoardDiv = document.getElementById("gameBoard");
const finishDiv = document.getElementById("finish");
const answerDiv = document.getElementById("questionAnswer");
const questionDiv = document.getElementById("questionDiv");
let absentPlayersDiv = document.getElementById("absentPlayersFinal");

let isTeamsSelected = false;
let isTopicSelected = false;
let isGameStarted = false;
let team01Score = 0;
let team02Score = 0;
let selectedTeamName = "no-class";
var rowsNb = 4;
var colsNb = 4;
let nbGroups = 2;
var data = "A B C D E F G H I J K L M N O P Q R S T U V W Y Z";
var alphabet = data.split(" ");
var cloneAlphabet;

window.onload = function () {
  buildBoard(rowsNb, colsNb);
};

function startGame() {
  if (newGame()) {
    hide(configDiv);
    isGameStarted = true;
  } else {
    hide(configDiv);
  }
  drawTeams(nbGroups);
}

function newGame() {
  if (isGameStarted) {
    return confirm(lang["newGame"]);
  } else {
    return true;
  }
}

function setRows(event, nb) {
  document.querySelector(".rowBtn.selected").classList.remove("selected");
  event.target.classList.add("selected");
  rowsNb = nb;
  colsNb = document.querySelector(".colBtn.selected").textContent;
  buildBoard(rowsNb, colsNb);
}

function setCols(event, nb) {
  document.querySelector(".colBtn.selected").classList.remove("selected");
  event.target.classList.add("selected");
  colsNb = nb;
  buildBoard(rowsNb, colsNb);
}

function buildBoard(rows = 4, cols = 4) {
  cloneAlphabet = [];
  cloneAlphabet = structuredClone(alphabet);
  gameBoardDiv.innerHTML = "";
  for (var i = 0; i < rows; i++) {
    var newLine = document.createElement("tr");
    gameBoardDiv.appendChild(newLine);
    for (var j = 0; j < cols; j++) {
      let newCell = document.createElement("td");
      newCell.className = "letter";
      newCell.addEventListener("click", selectCell);
      var randomLetter = pickRandomLetter();
      newCell.textContent = randomLetter;
      newLine.appendChild(newCell);
    }
  }
}

function pickRandomLetter() {
  var randomnumber = Math.floor(Math.random() * cloneAlphabet.length);
  var picked = cloneAlphabet[randomnumber];
  cloneAlphabet.splice(randomnumber, 1);
  return picked;
}

function selectCell(event) {
  if (event.target.classList.contains("letterSelected")) {
    event.target.classList.toggle("letterSelected");
  } else {
    let selectedCell = document.querySelector(".letterSelected");
    if (selectedCell != null) {
      selectedCell.classList.remove("letterSelected");
    }
    event.target.classList.toggle("letterSelected");
  }
}

function endGame() {
  audioApplause.play();
  showFinalBoard();
  return false;
}

function initFinalBoard() {
  hideAll();
  document
    .getElementById("absentPlayersFinal")
    .querySelectorAll("li")
    .forEach(function (el) {
      el.remove();
    });
  selectedPlayers.sort();
  document.getElementById("winningTeam").innerHTML = selectedTeamName;
}

function showFinalBoard() {
  initFinalBoard();
  let winningTeamListDiv = document.getElementById("winningTeamList");
  let allScorersDiv = document.getElementById("allScorersList");
  let absentPlayersDiv = document.getElementById("absentPlayersFinal");
  let absentPlayers = allPlayers.filter(
    (player) => player.team == selectedTeamName && player.isAbsent == 1,
  );
  let winningPlayers, allScorers;
  document.getElementById("endTitle").innerHTML =
    lang["finalScoreTitle"] + team01Score + " - " + team02Score;
  document.getElementById("endSubTitle").innerHTML = lang["finalScoreSubtitle"];
  if (team01Score > team02Score) {
    document.getElementById("endSubTitle").innerHTML = lang["congrat01"];
    winningPlayers = selectedPlayers.filter(
      (player) => player.gameTeam == 1 && player.isAbsent == 0,
    );
    isTie = false;
  } else if (team02Score > team01Score) {
    document.getElementById("endSubTitle").innerHTML = lang["congrat02"];
    winningPlayers = selectedPlayers.filter(
      (player) => player.gameTeam == 2 && player.isAbsent == 0,
    );
    isTie = false;
  } else {
    document.getElementById("endSubTitle").innerHTML = lang["tie"];
    isTie = true;
  }
  allScorers = selectedPlayers.filter((player) => player.score > 0);
  if (!isTie) {
    winningPlayers.sort((a, b) => a.name.localeCompare(b.name));
    allScorers.sort((a, b) => a.name.localeCompare(b.name));
    winningPlayers.forEach(function (player) {
      winningTeamListDiv.insertAdjacentHTML(
        "beforeend",
        "<li>" + player.name + "</li>",
      );
    });
    if (allScorers.length > 0) {
      allScorers.forEach(function (player) {
        if (player.score == 1) {
          var ptStr = "pt";
        } else {
          var ptStr = "pts";
        }
        allScorersDiv.insertAdjacentHTML(
          "beforeend",
          "<li>" +
            player.name +
            ' <span class="goal">[' +
            player.score +
            ptStr +
            "]</span></li>",
        );
      });
    }
  } else {
    winningTeamListDiv.insertAdjacentHTML(
      "beforeend",
      "<li>" + lang["nobody"] + "</li>",
    );
    if (allScorers.length > 0) {
      allScorers.forEach(function (player) {
        if (player.score == 1) {
          var ptStr = "pt";
        } else {
          var ptStr = "pts";
        }
        losingScorersDiv.insertAdjacentHTML(
          "beforeend",
          "<li>" +
            player.name +
            ' <span class="goal">[' +
            player.score +
            ptStr +
            "]</span></li>",
        );
      });
    }
  }
  if (absentPlayers.length > 0) {
    absentPlayers.forEach(function (player) {
      absentPlayersDiv.insertAdjacentHTML(
        "beforeend",
        "<li>" + player.name + "</li>",
      );
    });
  } else {
    absentPlayersDiv.insertAdjacentHTML(
      "beforeend",
      "<li>" + lang["nobody"] + "</li>",
    );
  }
  toggle(endDiv);
  // Store in localStorage ?
  saveToLocalStorage();
}

function actionMenu(event, playerId) {
  let parent = event.target.parentElement;
  if (parent.classList.contains("onGameBoard")) {
    // event.target.classList.toggle("selectedPlayer");
    let selectedEl = event.target;
    let playerId = selectedEl.getAttribute("data-playerId");
    selectedEl.classList.toggle("selectedPlayer");
    if (selectedEl.classList.contains("selectedPlayer")) {
      selectedEl.insertAdjacentHTML(
        "afterbegin",
        "<span class='scorer' onclick='validScorer(" +
          playerId +
          ");'>✓</span> ",
      );
    } else {
      selectedEl.querySelector(".scorer").remove();
    }
  } else {
    let player = allPlayers.find((player) => player.id == playerId);
    document.getElementById("actionSelectedPlayer").innerHTML = player.name;
    document
      .getElementById("actionSelectedPlayer")
      .setAttribute("data-playerId", player.id);
    document
      .getElementById("actionSelectedPlayer")
      .setAttribute("data-team", player.team);
    show(actionDiv, false, true);
  }
}

function action(type, sender = null) {
  let playerId = document
    .getElementById("actionSelectedPlayer")
    .getAttribute("data-playerId");
  let player = allPlayers.find((player) => player.id == playerId);
  const parentTeam01 = document.getElementById("team01Players");
  const parentTeam02 = document.getElementById("team02Players");
  let playerDiv = document.getElementById("pl-" + player.id);
  switch (type) {
    case "switchTeam":
      if (parentTeam01.contains(playerDiv)) {
        // console.log('Team 01 > Team 02');
        parentTeam02.appendChild(playerDiv);
        sortList(parentTeam02);
        player.gameTeam = 2;
      } else {
        // console.log('Team 02 > Team 01');
        parentTeam01.appendChild(playerDiv);
        sortList(parentTeam01);
        player.gameTeam = 1;
      }
      break;
    case "toggleAbsent":
      let checkBox = document.getElementById("cb-" + playerId);
      checkBox.click();
      break;
    default:
      break;
  }
  hide(actionDiv);
  if (sender != null) {
    show(sender);
  }
}

function pickPlayer(nb) {
  resetPlayers(); // Reset players active state
  // Pick nb random players in each team
  if (isTeamsSelected) {
    document.querySelectorAll(".scorer").forEach(function (el) {
      el.remove();
    });
    for (var t = 1; t < 3; t++) {
      // Select from both teams
      if (nb > 0) {
        for (var i = 0; i < nb; i++) {
          const teamPlayers = allPlayers.filter(
            (player) =>
              player.team == selectedTeamName &&
              player.gameTeam == t &&
              player.isAbsent == 0 &&
              player.active == 1,
          );
          const random = Math.floor(Math.random() * teamPlayers.length);
          let selectedPlayer = teamPlayers[random];
          selectedPlayer.active = 0;
          let selectedEl = document.getElementById("pl-" + selectedPlayer.id);
          selectedEl.classList.add("selectedPlayer");
          selectedEl.insertAdjacentHTML(
            "afterbegin",
            "<span class='scorer' onclick='validScorer(" +
              selectedPlayer.id +
              ");'>✓</span> ",
          );
        }
      } else {
        const teamPlayers = allPlayers.filter(
          (player) =>
            player.team == selectedTeamName &&
            player.gameTeam == t &&
            player.isAbsent == 0 &&
            player.active == 1,
        );
        for (var i = 0; i < teamPlayers.length; i++) {
          let selectedEl = document.getElementById("pl-" + teamPlayers[i].id);
          selectedEl.classList.add("selectedPlayer");
          selectedEl.insertAdjacentHTML(
            "afterbegin",
            "<span class='scorer' onclick='validScorer(" +
              teamPlayers[i].id +
              ");'>✓</span> ",
          );
        }
      }
    }
  }
}

function validScorer(playerId) {
  let selectedCell = document.querySelector(".letterSelected");
  if (selectedCell != null) {
    selectedCell.textContent = "";
    selectedCell.classList.remove("letterSelected");
    selectedCell.classList.add("done");
    // window.alert(event.target.parentNode.getAttribute("id"));
    let scorer = selectedPlayers.filter((player) => player.id == playerId)[0];
    scorer.score++;
    document.querySelectorAll(".scorer").forEach(function (el) {
      el.remove();
    });
    document.querySelectorAll(".selectedPlayer").forEach(function (el) {
      el.classList.remove("selectedPlayer");
    });
    if (scorer.gameTeam == 1) {
      team01Score++;
    } else {
      team02Score++;
    }
    if (document.querySelectorAll("td.letter:not(.done)").length == 0) {
      endGame();
    }
  }
  event.stopPropagation();
}

function resetPlayers() {
  const teamPlayers = allPlayers.filter(
    (player) => player.team == selectedTeamName && player.isAbsent == 0,
  );
  teamPlayers.forEach(function (player) {
    player.active = 1;
  });
  document.querySelectorAll("li.selectedPlayer").forEach(function (el) {
    el.classList.remove("selectedPlayer");
  });
}
function resetBoard(confirmMessage = false) {
  // TODO confirmAction() ???
  if (confirmMessage == true) {
    if (confirm(lang["resetBoard"])) {
      if (isTeamsSelected) {
        buildBoard(rowsNb, colsNb);
      }
    }
  } else {
    return false;
  }
}
