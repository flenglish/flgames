// Global variables
allSavedGames = localStorage.getItem("flgames");

const audioApplause = new Audio("../../assets/audio/applause.ogg");

const gameBoardDiv = document.getElementById("gameBoard");
const finishDiv = document.getElementById("finish");
const answerDiv = document.getElementById("answerDiv");
const questionDiv = document.getElementById("questionDiv");
let absentPlayersDiv = document.getElementById("absentPlayersFinal");

let isTeamsSelected = false;
let isTopicSelected = false;
let isGameStarted = false;
let selectedTeamName = "no-class";

window.addEventListener("load", () => {
  document.querySelectorAll(".scoreLabel").forEach((obj) => {
    addEvent(obj, "dblclick", () => {
      actionMenu(obj);
    });
  });
});

function startGame() {
  hide(configDiv);
  if (newGame()) {
    isGameStarted = true;
    drawQuestion();
  }
}

function actionMenu(obj) {
  document.getElementById("actionSelectedPlayer").innerHTML = obj.title;
  document
    .getElementById("actionSelectedPlayer")
    .setAttribute("data-id", obj.id);
  show(actionDiv, false, true);
}

function newGame() {
  if (isGameStarted) {
    return confirm(lang["newGame"]);
  } else {
    return true;
  }
}

function endGame() {
  audioApplause.play();
  showFinalBoard();
  return false;
}

function initFinalBoard() {
  hideAll();
  document
    .getElementById("absentPlayersFinal")
    .querySelectorAll("li")
    .forEach(function (el) {
      el.remove();
    });
  document.getElementById("winningTeam").innerHTML = selectedTeamName;
}

function showFinalBoard() {
  initFinalBoard();
  let winningTeamListDiv = document.getElementById("winningTeamList");
  winningTeamListDiv.innerHTML = "";
  let finalScoreTeam01 = document.getElementById("scoreTeam").textContent;
  let finalScoreTeam02 = document.getElementById("scoreTeacher").textContent;
  document.getElementById("endTitle").innerHTML =
    lang["finalScoreTitle"] +
    " : " +
    finalScoreTeam01 +
    " - " +
    finalScoreTeam02;
  if (finalScoreTeam01 > finalScoreTeam02) {
    document.getElementById("endSubTitle").innerHTML =
      lang["congratulationsTeam"];
    winningPlayers = allPlayers.filter(
      (player) => player.team == selectedTeamName && player.isAbsent == 0,
    );
    isTie = false;
  } else if (finalScoreTeam02 > finalScoreTeam01) {
    document.getElementById("endSubTitle").innerHTML =
      lang["congratulationsTeacher"];
    winningPlayers = [];
    isTie = false;
  } else {
    document.getElementById("endSubTitle").innerHTML = lang["tie"];
    winningPlayers = [];
    isTie = true;
  }
  if (!isTie) {
    if (finalScoreTeam01 > finalScoreTeam02) {
      winningPlayers.forEach(function (player) {
        winningTeamListDiv.insertAdjacentHTML(
          "beforeend",
          "<li>" + player.name + "</li>",
        );
      });
    } else {
      winningTeamListDiv.insertAdjacentHTML(
        "beforeend",
        "<li>" + lang["winningTeacher"] + "</li>",
      );
    }
  } else {
    winningTeamListDiv.insertAdjacentHTML(
      "beforeend",
      "<li>" + lang["nobody"] + "</li>",
    );
  }
  let absentPlayersDiv = document.getElementById("absentPlayersFinal");
  absentPlayers = allPlayers.filter(
    (player) => player.team == selectedTeamName && player.isAbsent == 1,
  );
  if (absentPlayers.length > 0) {
    absentPlayers.forEach(function (player) {
      absentPlayersDiv.insertAdjacentHTML(
        "beforeend",
        "<li>" + player.name + "</li>",
      );
    });
  } else {
    absentPlayersDiv.insertAdjacentHTML(
      "beforeend",
      "<li>" + lang["nobody"] + "</li>",
    );
  }
  document.getElementById("finalSentences").innerHTML =
    document.getElementById("savedAnswers").innerHTML;
  document
    .querySelectorAll("#finalSentences .closeIcon")
    .forEach(function (el) {
      el.remove();
    });
  document.querySelectorAll("#finalSentences .icons").forEach(function (el) {
    el.remove();
  });
  toggle(endDiv);
  saveToLocalStorage();
}

function showTeam(className) {
  unSelectAllPlayers();
  var selectPlayers = document.querySelectorAll(
    "li[data-team='" + className + "']",
  );
  selectPlayers.forEach(function (player) {
    player.classList.add("activePlayer");
  });
  // document.getElementById('selectedTeam').innerHTML = className;
  document.querySelectorAll(".btn.selectedTeam").forEach(function (el) {
    el.classList.remove("selectedTeam");
  });
  event.target.classList.add("selectedTeam");
  selectedTeamName = className;
  drawTeams(2, false);
}

function mixBoard() {
  drawQuestion();
}

function drawQuestion() {
  if (isTopicSelected) {
    document.getElementById("gameDiv").innerHTML = "";
    shuffle(selectedQuestions);
    for (var i = 0; i < selectedQuestions.length; i++) {
      // for (var i = 0; i < 15; i++) {
      let promptEl =
        "<span class='prompt'>" + selectedQuestions[i].question + "</span>";
      document.getElementById("gameDiv").innerHTML += promptEl;
    }
    show(gameDiv);
  }
}

function saveAnswer(type) {
  let answer = answerDiv.value; // Get textarea value
  if (type == 1) {
    // Right answer
    setScore(1, "scoreTeam");
    let answerText = document.getElementById("rightButton").innerText;
    document.getElementById("savedAnswers").innerHTML +=
      "<li><span class='rightAnswer'>" +
      answerText +
      "</span> → " +
      answer +
      ' <span class="closeIcon" onclick="deleteAnswer();">&#10006;</span>' +
      ' <span class="icons" onclick="setScore(1, \'scoreTeacher\');"><img src="../../assets/icons/thumb-down-filled.svg" alt="malus" /></span>' +
      ' <span class="icons" onclick="setScore(1, \'scoreTeam\');"><img src="../../assets/icons/thumb-up-filled.svg" alt="bonus" /></span></li>';
  } else {
    // Wrong answer
    setScore(1, "scoreTeacher");
    let answerText = document.getElementById("wrongButton").innerText;
    document.getElementById("savedAnswers").innerHTML +=
      "<li><span class='wrongAnswer'>" +
      answerText +
      "</span> → " +
      answer +
      ' <span class="closeIcon" onclick="deleteAnswer();">&#10006;</span></li>';
  }
  answerDiv.value = "";
  hide(questionDiv);
}

function setScore(value, id = false) {
  if (id == false) {
    id = document
      .getElementById("actionSelectedPlayer")
      .getAttribute("data-id");
  }
  let oldScore = parseInt(document.getElementById(id).innerHTML);
  document.getElementById(id).innerHTML = parseInt(oldScore + value);
}

function deleteAnswer() {
  event.target.parentNode.remove();
}
