var langLocal = {
  congratulationsTeam: "Félicitations à l'équipe !",
  congratulationsTeacher: "Désolé ! L'enseignant⋅e a gagné le combat !",
  addSentence: "Ajouter une phrase",
  showPrompts: "Montrer les mots à utiliser",
  reloadQuestions: "Recharger la liste de mots",
  writeText: "Écrivez votre phrase !",
  rightButton: "Juste",
  wrongButton: "Faux",
  scoreTeamLabel: "Équipe ",
  scoreTeacherLabel: "Prof ",
  finalSentences: "Phrases finales",
  help01: "Choisir un thème pour afficher les mots à utiliser.",
  help02:
    "Chaque élève écrit 2 ou 3 phrases en utilisant les mots proposés sur des petits bouts de papier différents.",
  help03:
    "Pas de nom ! Pas de papiers pliés ! Tous les papiers sont mis dans une boite.",
  help04:
    "L'enseignant⋅e prend au hasard un bout de papier et recopie la phrase qui s'y trouve (après avoir cliqué sur « Ajouter une phrase »).",
  help05: "Tout le monde participe pour corriger la phrase.",
  help06:
    "Cliquer sur la réponse appropriée (Juste / Faux) pour enregistrer la phrase.",
  help07:
    "Si la phrase est valide (pas une répétition, par exemple, suffisamment longue, tous les mots demandés sont utilisés…), un bonus peut être ajouté (pouce vers le haut) ou donné à l'enseignant⋅e (pouce vers le bas)",
  help08:
    "Un autre vout de papier est tiré au sort, et on continue jusqu'à ce que l'enseignant⋅e décide de la fin du jeu.",
  help09: "Jouez bien avec FLGames !",
};
