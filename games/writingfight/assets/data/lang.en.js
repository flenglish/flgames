var langLocal = {
  congratulationsTeam: "Congratulations to the team !",
  congratulationsTeacher: "Sorry ! The teacher has won the fight !",
  addSentence: "Add a sentence",
  showPrompts: "Show the prompts",
  reloadQuestions: "Reload prompts",
  writeText: "Write your text !",
  rightButton: "Right",
  wrongButton: "Wrong",
  scoreTeamLabel: "Team ",
  scoreTeacherLabel: "Teacher ",
  finalSentences: "Game sentences",
  help01: "Choose a topic to display prompts.",
  help02:
    "Each player writes 2 or 3 sentences using the prompts on different small pieces of paper.",
  help03: "No name ! No folding ! All pieces are put in a  box.",
  help04:
    "The teacher randomly picks a piece of paper and copy the written sentence on the board by clicking on “ Add a sentence ”.",
  help05: "Everybody tries and correct the sentence.",
  help06: "Click on the appropriate result (Right/Wrong) to save the sentence.",
  help07:
    "If the sentence is valid (not a repetition for example, long enough, all prompts parts used…), a bonus may be added (thumb up) or given to the teacher (thumb down).",
  help08:
    "Another piece of paper is picked, and so on until the teacher decides on the end of the game.",
  help09: "Have fun learning with FLGames !",
};
