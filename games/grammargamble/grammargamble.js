// Global variables
allSavedGames = localStorage.getItem("flgames");

const audioApplause = new Audio("../../assets/audio/applause.ogg");

const gameBoardDiv = document.getElementById("gameBoard");
const finishDiv = document.getElementById("finish");
const answerDiv = document.getElementById("questionAnswer");
const questionDiv = document.getElementById("questionDiv");
let absentPlayersDiv = document.getElementById("absentPlayersFinal");

let isTeamsSelected = false;
let isTopicSelected = false;
let isGameStarted = false;
let selectedTeamName = "no-class";

function startGame() {
  if (newGame()) {
    hide(configDiv);
    isGameStarted = true;
  } else {
    hide(configDiv);
  }
}

function newGame() {
  if (isGameStarted) {
    return confirm(lang["newGame"]);
  } else {
    return true;
  }
}

function endGame() {
  audioApplause.play();
  showFinalBoard();
  return false;
}

function initFinalBoard() {
  hideAll();
  document
    .getElementById("absentPlayersFinal")
    .querySelectorAll("li")
    .forEach(function (el) {
      el.remove();
    });
  selectedPlayers.sort();
  selectedPlayers.forEach(function (el) {
    if (el.isAbsent == 0) {
      let name = el.name.trim();
      document.querySelector("#endDiv select").innerHTML +=
        "<option onclick=\"selectWinner('" +
        name +
        "');\">" +
        name +
        "</option>";
    }
  });
  document.getElementById("winningTeam").innerHTML = selectedTeamName;
}

function selectWinner(name) {
  document.getElementById("selectedWinners").innerHTML +=
    "<li>" + name + "</li>";
  let list = document.querySelector("#endDiv select");
  list.options[list.selectedIndex].disabled = true;
  list.options[0].selected = true;
  removeFromLocalStorage();
  saveToLocalStorage();
}

function showFinalBoard() {
  initFinalBoard();
  let absentPlayersDiv = document.getElementById("absentPlayersFinal");
  absentPlayers = allPlayers.filter(
    (player) => player.team == selectedTeamName && player.isAbsent == 1,
  );
  document.getElementById("endTitle").innerHTML = lang["finalScoreTitle"];
  document.getElementById("endSubTitle").innerHTML = lang["finalScoreSubtitle"];
  if (absentPlayers.length > 0) {
    absentPlayers.forEach(function (player) {
      absentPlayersDiv.insertAdjacentHTML(
        "beforeend",
        "<li>" + player.name + "</li>",
      );
    });
  } else {
    absentPlayersDiv.insertAdjacentHTML(
      "beforeend",
      "<li>" + lang["nobody"] + "</li>",
    );
  }
  document.getElementById("finalSentences").innerHTML =
    document.getElementById("savedAnswers").innerHTML;
  document
    .querySelectorAll("#finalSentences .closeIcon")
    .forEach(function (el) {
      el.remove();
    });
  toggle(endDiv);
  // Store in localStorage ?
  saveToLocalStorage();
  // let toSave = '';
  // endDiv.querySelectorAll('#endDiv > .toSave').forEach( function (el) { toSave += el.innerHTML; });
  // toSave = '<div class="savedGame">' + toSave + '</div>';
  // let alreadySaved = localStorage.getItem('flgames');
  // if (alreadySaved != null) {
  //   toSave = alreadySaved + toSave;
  // }
  // localStorage.setItem('flgames', toSave);
}

function showTeam(className) {
  unSelectAllPlayers();
  var selectPlayers = document.querySelectorAll(
    "li[data-team='" + className + "']",
  );
  selectPlayers.forEach(function (player) {
    player.classList.add("activePlayer");
  });
  // document.getElementById('selectedTeam').innerHTML = className;
  document.querySelectorAll(".btn.selectedTeam").forEach(function (el) {
    el.classList.remove("selectedTeam");
  });
  event.target.classList.add("selectedTeam");
  selectedTeamName = className;
  drawTeams(2, false);
}

function drawQuestion() {
  if (isTopicSelected) {
    initQuestion();
    let currentQuestion = document.getElementById("question");
    // Init question
    let question = false;
    let questionAnswer = false;
    let questionExolained = false;
    let questionParts = [];
    // Pick a random question
    const random = Math.floor(Math.random() * selectedQuestions.length);
    let randQuestion = selectedQuestions[random].question;
    // Parse selected question for timer and answer
    questionParts = randQuestion.split("::");
    question = questionParts[0].trim();
    if (questionParts[1]) {
      if (questionParts[1].toLowerCase().trim() === "right") {
        questionAnswer =
          '<span class="rightAnswer">' + questionParts[1].trim() + "</span>";
      } else {
        questionAnswer =
          '<span class="wrongAnswer">' + questionParts[1].trim() + "</span>";
      }
    } else {
      questionAnswer = "---";
    }
    if (questionParts[2]) {
      questionExolained = questionParts[2].trim();
    } else {
      questionExolained = "---";
    }
    currentQuestion.innerHTML = question;
    if (questionExolained !== "---") {
      questionAnswer += " → " + questionExolained;
    }
    answerDiv.innerHTML = questionAnswer;
    show(questionDiv);
  }
}

function initQuestion() {
  hideAll();
  // audioStart.play();
  document.getElementById("question").innerHTML = "";
  answerDiv.classList.add("hidden");
  answerDiv.innerHTML = "";
  document.getElementById("saveAnswerButton").classList.add("hidden");
}

function toggleAnswer() {
  if (answerDiv.classList.contains("hidden")) {
    answerDiv.classList.remove("hidden");
    document.getElementById("saveAnswerButton").classList.remove("hidden");
  } else {
    answerDiv.classList.add("hidden");
    document.getElementById("saveAnswerButton").classList.add("hidden");
  }
}

function saveAnswer() {
  let answer = answerDiv.innerText;
  if (answer.toLowerCase() === "right") {
    document.getElementById("savedAnswers").innerHTML +=
      "<li>" +
      answerDiv.innerHTML +
      " → " +
      question.innerHTML +
      ' <span class="closeIcon" onclick="deleteAnswer();">&#10006;</span></li>';
  } else {
    document.getElementById("savedAnswers").innerHTML +=
      "<li>" +
      answerDiv.innerHTML +
      ' <span class="closeIcon" onclick="deleteAnswer();">&#10006;</span></li>';
  }
  hide(questionDiv);
}

function deleteAnswer() {
  event.target.parentNode.remove();
}
