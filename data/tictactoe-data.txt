// ----------------------------------------------------------
// Data format :
// A new topic starts with ': Topic Title'
// Each line is what will be randomly displayed in a boardgame cell
// Images can be inserted using syntax : [my-file.png] and the file must be placed in "flgames/data/img/" directory.
// You need to have at least 9 lines in a topic
// ---
// Format des données :
// Un nouveau thème commence par « : Titre du thème »
// Chaque ligne correspond à ce qui s'affichera (de manière aléatoire) dans une case du jeu
// Des images peuvent être utilisées avec la syntaxe : [mon-image.png] et le fichier devra être mis dans le dossier « flgames/data/img/ ».
// Il faut avoir au moins 9 lignes dans un thème
// ----------------------------------------------------------

: Numbers in TEEN-TY
13
30
14
40
15
50
16
60
17
70
18
80
19
90

: Numbers in TY
10
20
30
40
50
60
70
80
90

: Numbers (Maths)
5 + 5
12 × 3
9 - 7
10 + 3 + 2 + 1
44 - 8
7 + 7
100 ÷ 2
100 ÷ 4
50 + 16
43 - 8
60 + 3 + 8
7 × 4
9 × 7
51 - 47
9 × 9
(4 × 7) ÷ 2

:Adjectives (easy)
angry
big
boring
dangerous
difficult
expensive
funny
happy
important
intelligent
interesting
lazy
long
old
sad
small
strong
tall
young

:BE/HAVE
What -- your name ?
How --- you ?
I -- a dog but no cat.
Lucie -- a nice girl.
Jack and Sam --- not serious !
My father -- a blue car.
Her brother -- strong.
My friends -- funny.
She -- glasses.
He -- tired.
They -- blue eyes.
I -- a good football player.
-- you good at football ?
-- he absent ?
-- you got a pet ?
-- they in your class ?

:Days, Months, Seasons
Lundi
Mardi
Mercredi
Jeudi
Vendredi
Samedi
Dimanche
Janvier
Février
Mars
Avril
Mai
Juin
Juillet
Août
Septembre
Octobre
Novembre
Décembre
Printemps
Été
Hiver
Automne

: Describing a picture
[describing_picture/in_the_top_left_hand_corner.png]
[describing_picture/in_the_top_right_hand_corner.png]
[describing_picture/at_the_bottom.png]
[describing_picture/at_the_top.png]
[describing_picture/in_the_background.png]
[describing_picture/in_the_foreground.png]
[describing_picture/in_the_middle.png]
[describing_picture/in_the_bottom_left_hand_corner.png]
[describing_picture/in_the_bottom_right_hand_corner.png]
[describing_picture/on_the_left.png]
[describing_picture/on_the_right.png]

: ED Verbs
worked
watched
listened
played
started
missed
arrived
received
walked
yelled
slipped
finished
washed
turned
opened
closed
liked
loved
hated

:Talking about experiences
taken the plane
eaten insects
touched a snake
done bungee-jumping
done snowboarding
done sandboarding
done kite-surfing
driven a tractor
ridden a motorbike
played Call of Duty
called the police
been to London
swum in the Atlantic ocean.
surfed


: Flags 01
[flags/australia.png]
[flags/canada.png]
[flags/england.png]
[flags/france.png]
[flags/ireland.png]
[flags/scotland.png]
[flags/united_kingdom.png]
[flags/usa.png]
[flags/wales.png]
[flags/india.png]
[flags/south_africa.png]
[flags/new_zealand.png]

:Frequency expressions
1 fois par semaine
1 fois par mois
1 fois par jour
1 fois par an
2 fois par mois
2 fois par jour
2 fois par an
3 fois par semaine
3 fois par mois
3 fois par jour
5 fois par an
10 fois par jour
15 fois par seconde
20 fois par jour
50 fois par jour
jamais
toujours
souvent
parfois

:Irregular verbs 01
be
être
break
casser
come
venir
do
faire
drink
boire
eat
manger
find
trouver
get
obtenir|devenir
go
aller
have
avoir
meet
rencontrer
put
poser|mettre
read
lire
ride
faire du vélo|du cheval
run
courir
see
voir
sit
s'asseoir
sleep
dormir
speak
parler
swim
nager
take
prendre
say
dire
think
penser
understand
comprendre
write
écrire

:Can - Can't - Likes
[can_cant_likes/he_can_draw.png]
[can_cant_likes/she_can_play_tennis.png]
[can_cant_likes/he_can_play_football.png]
[can_cant_likes/she_can_ride_horse.png]
[can_cant_likes/he_cant_ski.png]
[can_cant_likes/she_cant_play_rugby.png]
[can_cant_likes/he_cant_surf.png]
[can_cant_likes/she_cant_swim.png]
[can_cant_likes/he_dont_like_sing.png]
[can_cant_likes/she_dont_like_draw.png]
[can_cant_likes/he_hate_ride_bike.png]
[can_cant_likes/she_hate_swim.png]
[can_cant_likes/she_like_ride_bike.png]
[can_cant_likes/she_love_ride_horse.png]
[can_cant_likes/he_love_play_football.png]
[can_cant_likes/they_can_ride_horse.png]
[can_cant_likes/i_can_ride_horse.png]
[can_cant_likes/they_cant_play_rugby.png]
[can_cant_likes/i_cant_sing.png]
[can_cant_likes/they_hate_surf.png]
[can_cant_likes/i_hate_rugby.png]
[can_cant_likes/they_like_play_tennis.png]
[can_cant_likes/i_like_swim.png]

: Possessives
ma soeur
mon frère
mes amis
son père (à Sarah)
son père (à Mike)
sa mère (à Sarah)
sa mère (à Mike)
leurs parents
leur grand-mère
votre grand-père
votre cousine
ton cousin
ta soeur
ton frère
tes parents
tes amis
vos chats
son chien (à Simon)
son chat (à John)
son chat (à Cindy)
son chien (à Tracy)

: Present perfect
I (never) (ride) a horse.
She (already) (eat) snails.
I (already) (take) the plane.
They (never) (be) to England.
Mike (never) (go) to Spain.
(ever) (be) to the USA ?
(ever) (drive) a sports car ?
(ever) (visit) Paris ?
(ever) (eat) frog legs ?
She (never) (see) the ocean.
I (never) (see) this man.
My parents and I (already) (go) to Germany.
She (never) (drive) a tractor.
He (never) (ride) a motorbike.
I (just) (finish) my work !
She (just) (tell) me.
They (just) (do) their exercise.

: What time is it ?
16:30
12:00
5:15
3:00
4:45
9:00
6:30
2:30
1:15
18:45
6:00
3:30
15:15
17:00
11:30
10:00
9:15
2:00
5:30
7:00
8:00
10:45
12:30
13:00
13:45
2:00
4:00
6:00
7:15
9:15
9:50
8:45
4:20pm
11:05am
12:00
2:30

:WH Questions (prétérit)
Où es-tu allé ?
Où est-il allé ?
Où est-elle allée ?
Où sont-ils allés ?
Qu'a t-elle fait ?
Qu'est-ce-que tu as fait ?
Qu'est-ce-qu'il a fait ?
Qu'est-ce-qu'elles ont fait ?
Qu'ont-ils écrit ?
Qu'as-tu écrit ?
Qu'a-t-il écrit ?
Qu'est-ce-qu'elle a vu ?
Qu'a t-il vu ?
Qui a t-il vu ?
Qu'avez-vous vu ?
Qui avez-vous vu ?
Qu'ont t-ils vu ?
Qui as-tu rencontré ?
Qui ont t-ils rencontrés ?
Qu'est-ce que tu as dit ?
Qu'est-ce qu'il a dit ?
Qu'a t'elle dit ?
Qu'ont ils dit ?
Qu'as tu acheté ?
Qu'a t-il acheté ?
Qu'est-ce qu'elle a écrit ?
Qu'est-ce que tu as mangé ?
Qu'est-ce qu'elle a lu ?
Quand est-il venu ?
Quand sommes-nous venus ?
Comment êtes-vous venus ?
Comment l'as-tu cassé ?
Comment l'avez-vous trouvé ?

: Question words
Qui ?
Quand ?
Où ?
Comment ?
Quoi ? Qu'est-ce-que ?
À quelle heure ?
Pourquoi ?
Quel âge ?
Who ?
Where ?
When ?
How old ?
What time ?
À qui ?
Why ?
How ?
What ?
Avez-vous…?
Êtes-vous…?
Aimez-vous…?
