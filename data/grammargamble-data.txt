// ----------------------------------------------------------
// Data format :
// A new topic starts with ': Topic Title'
// Each line is a question and each part (question/answer (right or wrong)/info) is separated by '::'.
// ie. : my question. :: Right/Wrong :: My answer explained
// ----------------------------------------------------------
// Format des données :
// Un nouveau thème commence par « : Titre du thème »
// Chaque ligne correspond à une question et chaque partie (question/réponse (vrai/faux)/info) est séparée par « :: ».
// ex. : ma question. :: Vrai/Faux :: ma réponse expliquée
// ----------------------------------------------------------

: Identity
My names Mike ! :: Wrong :: My name<u>'</u>s Mike !
I haven't any brothers. :: Wrong :: I haven't <u>got</u> any...
I have two cats and three dogs ! :: Right
What do you like doing in you spare time ? :: Wrong :: you<u>r</u> spare time
What your name ? :: Wrong :: What<u>'s</u> your name ?
How old are you ? :: Right
When are you born ? :: Wrong :: When <u>were</u> you born ?
Do you have any pets ? :: Right
Have you got any pets ? :: Right
Have you any sisters ? :: Wrong :: Have you <u>got</u>...?
Do you any pets ? :: Wrong :: Do you <u>have</u> any...?
My brother has 10. :: Wrong :: My brother <u>is</u> 10.
She live in London. :: Wrong :: She live<u>s</u> in London.
I'm french and I live in Raon. :: Wrong :: I'm <u>F</u>rench.
I was born on May 5, 2004. :: Right
He was born on June 12, 2003. :: Right
What's you phone number ? :: Wrong :: What's you<u>r</u> phone number ?
What's your nationality ? :: Right
I don't have any brothers or sisters. :: Right
I've got a dog. His name is Rex. :: Right
I have a sister. She's name is Melinda. :: Wrong :: sister → <u>Her</u> name is...

